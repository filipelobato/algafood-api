# Listar conexões com o banco de dados via terminal, executar no diretório {MYSQL}\bin 

mysqladmin -u [usuario] -p -i [tempo_atualizacao]
mysqladmin -u root -p -i 1 processlist

# Testando vários conexões simultâneas com Apache Bench
ab -n [numero_requisicoes] -c [numero_requisicoes_concorrentes] [URL]

# Dump com script criando schema, estrutura de tabelas e dados
mysqldump --host localhost --user root --password --databases algafood --add-drop-database --result-file=algafood-backup.sql

# Restore do schema algafood 
# 1 Login
mysql --host localhost --user root --password
# 2 Execute scripts
source algafood-backup.sql
