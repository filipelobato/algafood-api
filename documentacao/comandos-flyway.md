Formas de reparar migrações com erros

1ª Excluindo registro
1 - Exclui registro com erro da tabela de histórico do flyway (flyway_schema_history)
2 - Desfazer alterações que foram efetivadas pelo arquivo
3 - Roda novamente a aplicação.

2ª Linha de comando 
Descrição: ideal para quando não se tem acesso ao banco, mas a aplicação tem arquivo de config
com as propriedades do banco de dados.
1 - Cria arquivo flyway.properties com as propriedades do banco de dados:
flyway.url= 
flyway.user=
flyway.password=
2 - mvnw flyway:repair -Dflyway.configFiles=src/main/resources/flyway.properties

O flyway.properties só será utilizado quando for pela linha de comando