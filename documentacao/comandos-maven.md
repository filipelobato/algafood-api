# verificar testes de integração
mvnw verify
# select string em dependências (Windows)
mvnw dependency:tree | Select-String -Pattern hamcrest

# realizar testes unitários
mvnw test

# Sonarqube
mvnw sonar:sonar -Dsonar.projectKey=algafood-api -Dsonar.host.url=http://localhost:9000 -Dsonar.login=d67d0a6a122df5f176251b321a20a3c066bc18cc
