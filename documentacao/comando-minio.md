# Comando para subir um container de MinIO (Emula um S3) #

docker run -p 9000:9000 -p 9001:9001 --name minio1 -v C:\Desenvolvimento\minio\data:/data -e "MINIO_ROOT_USER=root" -e "MINIO_ROOT_PASSWORD=rootrootroot" quay.io/minio/minio server data --console-address ":9001"

{
  "Rules": [
    {
      "ID": "DeleteTemporaryFilesAfter7Days",
      "Status": "Enabled",
      "Expiration": {
        "Days": 7
      },
      "Filter": {
        "Tag": {
          "Key": "temporario",
          "Value": "true"
        }
      }
    }
  ]
}

docker cp lifecycle.json minio:/tmp/lifecycle.json

docker exec -it meu-minio /bin/sh
mc alias set myminio http://127.0.0.1:9000 MEU_ACESSO MEU_SEGREDO

myminio → Nome do alias (pode ser qualquer coisa, como minio-local ou minio-servidor).
http://127.0.0.1:9000 → URL do seu MinIO.
MEU_ACESSO → Sua Access Key do MinIO.
MEU_SEGREDO → Sua Secret Key do MinIO.

Aplicar a regra ao bucket:
mc ilm import myminio/algafood-s3 < /tmp/lifecycle.json

Para verificar se a regra foi configurada corretamente, rode:
mc ilm export myminio/meu-bucket