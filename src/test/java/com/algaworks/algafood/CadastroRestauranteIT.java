package com.algaworks.algafood;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.comparesEqualTo;

import java.math.BigDecimal;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.algaworks.algafood.domain.model.Cidade;
import com.algaworks.algafood.domain.model.Cozinha;
import com.algaworks.algafood.domain.model.Estado;
import com.algaworks.algafood.domain.model.Restaurante;
import com.algaworks.algafood.domain.repository.CidadeRepository;
import com.algaworks.algafood.domain.repository.CozinhaRepository;
import com.algaworks.algafood.domain.repository.EstadoRepository;
import com.algaworks.algafood.domain.repository.RestauranteRepository;
import com.algaworks.algafood.util.DatabaseCleaner;
import com.algaworks.algafood.util.ResourceUtils;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("/application-test.properties")
public class CadastroRestauranteIT {
	
	private static final String VIOLACAO_DE_REGRA_DE_NEGOCIO_PROBLEM_TYPE = "Violação de regra de negócio";
	
	private static final String DADOS_INVALIDOS_PROBLEM_TITLE = "Dados inválidos";
	
	@LocalServerPort
	private int port;
	
	@Autowired
	private DatabaseCleaner databaseCleaner;
	
	@Autowired
	private RestauranteRepository restauranteRepository;
	
	@Autowired
	private CozinhaRepository cozinhaRepository;
	
	@Autowired
	private EstadoRepository estadoRepository;
	
	@Autowired
	private CidadeRepository cidadeRepository;
	
	private static final int RESTAURANTE_ID_INEXISTENTE = 100;

	private Restaurante restauranteJavaSteakhouse;
	private String jsonRestauranteCorreto;
	private String jsonRestauranteSemCozinha;
	private String jsonRestauranteSemFrete;
	private String jsonRestauranteComCozinhaInexistente;

	@BeforeEach
	public void setUp() {
		RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
		RestAssured.port = port;
		RestAssured.basePath = "/restaurantes";
		jsonRestauranteCorreto = ResourceUtils.getContentFromResource(
				"/json/correto/restaurante-java-steakhouse.json");
		jsonRestauranteSemCozinha = ResourceUtils.getContentFromResource(
				"/json/incorreto/restaurante-java-steakhouse-sem-cozinha.json");
		jsonRestauranteSemFrete = ResourceUtils.getContentFromResource(
				"/json/incorreto/restaurante-java-steakhouse-sem-frete.json");
		jsonRestauranteComCozinhaInexistente = ResourceUtils.getContentFromResource(
				"/json/incorreto/restaurante-java-steakhouse-com-cozinha-inexistente.json");
		
		databaseCleaner.clearTables();
		prepararDados();
	}
	
	private void prepararDados() {
		Cozinha cozinhaTailandesa = new Cozinha();
		cozinhaTailandesa.setNome("Tailandesa");
		cozinhaTailandesa = cozinhaRepository.save(cozinhaTailandesa);
		
		restauranteJavaSteakhouse = new Restaurante();
		restauranteJavaSteakhouse.setNome("Java Steakhouse");
		restauranteJavaSteakhouse.setCozinha(cozinhaTailandesa);
		restauranteJavaSteakhouse.setTaxaFrete(new BigDecimal(10));
		restauranteJavaSteakhouse = restauranteRepository.save(restauranteJavaSteakhouse);
		
		Cidade cidadeSaoPaulo = new Cidade();
		cidadeSaoPaulo.setNome("São Paulo");
		
		Estado estadoSaoPaulo = new Estado();
		estadoSaoPaulo.setNome("São Paulo");
		
		estadoSaoPaulo = estadoRepository.save(estadoSaoPaulo);
		cidadeSaoPaulo.setEstado(estadoSaoPaulo);
		cidadeRepository.save(cidadeSaoPaulo);
	}

	@Test
	public void deveRetornarStatus200_QuandoConsultarRestauranteExistente() {
		given()
			.pathParam("restauranteId", restauranteJavaSteakhouse.getId())
			.accept(ContentType.JSON)
		.when()
			.get("{restauranteId}")
		.then()
			.statusCode(HttpStatus.OK.value());
	}
	
	@Test
	public void deveRetornarStatus201_QuandoCadastrarRestaurante() {
		given()
			.body(jsonRestauranteCorreto)
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
		.when()
			.post()
		.then()
			.statusCode(HttpStatus.CREATED.value());
	}
	

	@Test
	public void deveRetornarStatus404_QuandoCadastrarInexistente() {
		given()
			.pathParam("restauranteId", RESTAURANTE_ID_INEXISTENTE)
			.accept(ContentType.JSON)
		.when()
			.get("{restauranteId}")
		.then()
			.statusCode(HttpStatus.NOT_FOUND.value());
	}
	
	public void deveRetornarRespostaCorreta_QuandoConsultarRestauranteExistente() {
		given()
			.pathParam("restauranteId", restauranteJavaSteakhouse.getId())
			.accept(ContentType.JSON)
		.when()
			.get("{restauranteId}")
		.then()
			.body("taxaFrete", comparesEqualTo(restauranteJavaSteakhouse.getTaxaFrete().floatValue()));
	}
	
	@Test
	public void deveRetornarStatus400_QuandoCadastrarRestauranteSemCozinha() {
		given()
			.body(jsonRestauranteSemCozinha)
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
		.when()
			.post()
		.then()
			.statusCode(HttpStatus.BAD_REQUEST.value())
			.body("title", equalTo(DADOS_INVALIDOS_PROBLEM_TITLE));
	}
	
	@Test
	public void deveRetornarStatus400_QuandoCadastrarRestauranteSemFrete() {
		given()
			.body(jsonRestauranteSemFrete)
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
		.when()
			.post()
		.then()
			.statusCode(HttpStatus.BAD_REQUEST.value())
			.body("title", equalTo(DADOS_INVALIDOS_PROBLEM_TITLE));
	}
	
	@Test
	public void deveRetornarStatus400_QuandoCadastrarRestauranteComCozinhaInexistente() {
		given()
			.body(jsonRestauranteComCozinhaInexistente)
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
		.when()
			.post()
		.then()
			.statusCode(HttpStatus.BAD_REQUEST.value())
			.body("title", equalTo(VIOLACAO_DE_REGRA_DE_NEGOCIO_PROBLEM_TYPE));
	}

}
