package com.algaworks.algafood;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;

import javax.validation.ConstraintViolationException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.algaworks.algafood.domain.exception.EntidadeEmUsoException;
import com.algaworks.algafood.domain.exception.EntidadeNaoEncontradaException;
import com.algaworks.algafood.domain.model.Cidade;
import com.algaworks.algafood.domain.model.Cozinha;
import com.algaworks.algafood.domain.model.Endereco;
import com.algaworks.algafood.domain.model.Estado;
import com.algaworks.algafood.domain.model.Restaurante;
import com.algaworks.algafood.domain.service.CadastroCidadeService;
import com.algaworks.algafood.domain.service.CadastroCozinhaService;
import com.algaworks.algafood.domain.service.CadastroEstadoService;
import com.algaworks.algafood.domain.service.CadastroRestauranteService;

/** Teste de Integração */
@ExtendWith(SpringExtension.class)
@SpringBootTest
public class CadastroCozinhaITOld {

	@Autowired
	private CadastroCozinhaService cadastroCozinha;
	
	@Autowired
	private CadastroRestauranteService cadastroRestaurante;
	
	@Autowired
	private CadastroCidadeService cadastroCidade;
	
	@Autowired
	private CadastroEstadoService cadastroEstado;

	// Happy path
	@Test
	public void Salvar_DeveSalvar_QuandoNomeInformado() {
		// Cenário
		Cozinha novaCozinha = new Cozinha();
		novaCozinha.setNome("Chinesa");

		// ação
		novaCozinha = cadastroCozinha.salvar(novaCozinha);
		
		// validação
		assertThat(novaCozinha).isNotNull();
		assertThat(novaCozinha.getId()).isNotNull();
	}
	
	@Test
	public void Salvar_DeveFalhar_QuandoNomeForNulo() {
		Cozinha novaCozinha = new Cozinha();
		novaCozinha.setNome(null);

		ConstraintViolationException erroEsperado = Assertions
				.assertThrows(ConstraintViolationException.class, () -> {
			cadastroCozinha.salvar(novaCozinha);
		});

		assertThat(erroEsperado).isNotNull();
	}
	
	@Test
	public void Salvar_DeveFalhar_QuandoExcluirCozinhaEmUso() {
		Cozinha cozinha = new Cozinha();
		cozinha.setNome("Coreana");
		cozinha = cadastroCozinha.salvar(cozinha);
		
		Restaurante restaurante = new Restaurante();
		restaurante.setNome("Restaurante coreano");
		restaurante.setTaxaFrete(BigDecimal.TEN);
		restaurante.setCozinha(cozinha);
		
		Estado estado = new Estado();
		estado.setNome("Maranhão");
		estado = cadastroEstado.salvar(estado);
		
		Cidade cidade = new Cidade();
		cidade.setNome("São Luís");
		cidade.setEstado(estado);
		cidade = cadastroCidade.salvar(cidade);
		
		restaurante.setEndereco(new Endereco());
		restaurante.getEndereco().setCidade(cidade);
		restaurante = cadastroRestaurante.salvar(restaurante);
		
		Long cozinhaEmUsoId = cozinha.getId();

		EntidadeEmUsoException erroEsperado = Assertions
				.assertThrows(EntidadeEmUsoException.class, () -> {
			cadastroCozinha.excluir(cozinhaEmUsoId);
		});

		assertThat(erroEsperado).isNotNull();
	}
	
	@Test
	public void Salvar_DeveFalhar_QuandoExcluirCozinhaInexistente() {
		Long cozinhaInexistenteId = 100L;

		EntidadeNaoEncontradaException erroEsperado = Assertions
				.assertThrows(EntidadeNaoEncontradaException.class, () -> {
			cadastroCozinha.excluir(cozinhaInexistenteId);
		});

		assertThat(erroEsperado).isNotNull();
	}
}
