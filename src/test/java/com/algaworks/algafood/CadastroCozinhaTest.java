package com.algaworks.algafood;

import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.algaworks.algafood.domain.model.Cozinha;
import com.algaworks.algafood.domain.repository.CozinhaRepository;
import com.algaworks.algafood.domain.service.CadastroCozinhaService;

@ExtendWith(MockitoExtension.class)
public class CadastroCozinhaTest {

	@InjectMocks
	private CadastroCozinhaService cadastroCozinha;
	
	@Mock
	private CozinhaRepository cozinhaRepository;

	@Mock
	private EntityManager manager;

	@Mock
	private TypedQuery<Cozinha> query;

	@Test
	public void whenListCozinhaCreateQueryAndReturnResult() {
		Cozinha cozinha = new Cozinha();
		cozinha.setId(1L);
		cozinha.setNome("Tailandesa");

		Mockito.when(cozinhaRepository.findAll()).thenReturn(Arrays.asList(cozinha));

		List<Cozinha> cozinhas = cozinhaRepository.findAll();

		Assertions.assertThat(cozinhas)
			.hasSize(1)
			.first()
			.isSameAs(cozinha);

	}

}
