package com.algaworks.algafood.domain.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.algaworks.algafood.domain.repository.CozinhaRepository;

@ExtendWith(SpringExtension.class)
public class CadastroCozinhaTest {

	@Mock
	private CozinhaRepository repository;
	
	@InjectMocks
	private CadastroCozinhaService service;
	
	@Test
	public void shouldThrowNomePessoaInvalidoException_WhenNameIsInvalid() {
		//final Cozinha cozinha = new Cozinha(1L, "");
		//service.salvar(cozinha);
	}
}
