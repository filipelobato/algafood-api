package com.algaworks.algafood.domain.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.algaworks.algafood.domain.exception.FormaPagamentoNaoEncontradaException;
import com.algaworks.algafood.domain.exception.NegocioException;
import com.algaworks.algafood.domain.exception.PedidoNaoEncontradoException;
import com.algaworks.algafood.domain.exception.RestauranteNaoEncontradoException;
import com.algaworks.algafood.domain.model.Cidade;
import com.algaworks.algafood.domain.model.Endereco;
import com.algaworks.algafood.domain.model.FormaPagamento;
import com.algaworks.algafood.domain.model.ItemPedido;
import com.algaworks.algafood.domain.model.Pedido;
import com.algaworks.algafood.domain.model.Produto;
import com.algaworks.algafood.domain.model.Restaurante;
import com.algaworks.algafood.domain.model.StatusPedido;
import com.algaworks.algafood.domain.model.Usuario;
import com.algaworks.algafood.domain.repository.PedidoRepository;

@ExtendWith(SpringExtension.class)
public class EmissaoPedidoServiceTest {
	private EmissaoPedidoService emissaoPedidoService;
	
	@Mock
	private PedidoRepository pedidoRepository;
	
	@Mock
	private CadastroRestauranteService cadastroRestaurante;
	
	@Mock
	private CadastroFormaPagamentoService cadastroFormaPagamento;
	
	@Mock
	private CadastroCidadeService cadastroCidade;
	
	@Mock
	private CadastroUsuarioService cadastroUsuario;
	
	@Mock
	private CadastroProdutoService cadastroProduto;
	
	@BeforeEach
	public void setUp() {
		emissaoPedidoService = new EmissaoPedidoService(pedidoRepository, cadastroRestaurante, 
				cadastroFormaPagamento, cadastroCidade, cadastroUsuario, cadastroProduto);
	}
	
	@Test
	public void deveBuscarPedidoComSucesso_QuandoDadosEstiveremCorretos() {
		String codigoPedido = "24c6ec01-afb7-11ed-b94e-0242ac110002";
		Pedido pedido = new Pedido();
		
		Mockito.when(pedidoRepository.findByCodigo("24c6ec01-afb7-11ed-b94e-0242ac110002"))
			.thenReturn(java.util.Optional.of(pedido));
		
		Pedido resultado = emissaoPedidoService.buscarOuFalhar(codigoPedido);
		
		assertThat(resultado).isEqualTo(pedido);
	}
	
	@Test
	public void deveLancarExcecao_QuandoPedidoNaoForEncontrado() {
		String codigoPedido = "24c6ec01-afb7-11ed-b94e-0242ac110002";
		
		Mockito.when(pedidoRepository.findByCodigo(codigoPedido))
			.thenReturn(java.util.Optional.empty());
		
		Throwable exception = catchThrowable(() -> emissaoPedidoService.buscarOuFalhar(codigoPedido));
		
		assertThat(exception)
			.isInstanceOf(PedidoNaoEncontradoException.class)
			.hasMessage("Não existe um cadastro de pedido com código " + codigoPedido);
	}
	
	@Test
	public void deveEmitirPedidoComSucesso_QuandoDadosEstiveremCorretos() {
	    // criando os dados de teste
	    Restaurante restaurante = new Restaurante();
	    restaurante.setId(1L);
	    restaurante.setTaxaFrete(new BigDecimal("10.00"));
	    
	    FormaPagamento formaPagamento = new FormaPagamento();
	    formaPagamento.setId(1L);
	    
	    restaurante.adicionarFormaPagamento(formaPagamento);
	    
	    Cidade cidade = new Cidade();
	    cidade.setId(1L);
	    
	    Usuario cliente = new Usuario();
	    cliente.setId(1L);
	    
	    Produto produto = new Produto();
	    produto.setId(1L);
	    produto.setPreco(new BigDecimal("100.00"));
	    
	    ItemPedido item = new ItemPedido();
	    item.setProduto(produto);
	    item.setQuantidade(1);
	    
	    Produto produto2 = new Produto();
	    produto2.setId(2L);
	    produto2.setPreco(new BigDecimal("50.00"));
	    
	    ItemPedido item2 = new ItemPedido();
	    item2.setProduto(produto2);
	    item2.setQuantidade(1);
	    
	    Endereco endereco = new Endereco();
	    endereco.setCidade(cidade);
	    
	    Pedido pedido = new Pedido();
	    pedido.setId(1L);
	    pedido.setRestaurante(restaurante);
	    pedido.setFormaPagamento(formaPagamento);
	    pedido.setCliente(cliente);
	    pedido.setEnderecoEntrega(endereco);
	    pedido.setItens(Arrays.asList(item2));
	    pedido.setDataCriacao(OffsetDateTime.now());
	    
	    // configurando as dependências do serviço
	    when(cadastroRestaurante.buscarOuFalhar(restaurante.getId()))
        		.thenReturn(restaurante);

		when(cadastroFormaPagamento.buscarOuFalhar(formaPagamento.getId()))
		        .thenReturn(formaPagamento);
		
		when(cadastroCidade.buscarOuFalhar(cidade.getId()))
		        .thenReturn(cidade);
		
		when(cadastroUsuario.buscarOuFalhar(cliente.getId()))
		        .thenReturn(cliente);
		
		when(cadastroProduto.buscarOuFalhar(restaurante.getId(), produto2.getId()))
		        .thenReturn(produto2);
		
		when(pedidoRepository.save(pedido)).thenReturn(pedido);
	    
	    // invocando o método a ser testado
	    Pedido pedidoEmitido = emissaoPedidoService.emitir(pedido);
	    
	    // verificações    
	    assertThat(pedidoEmitido).isNotNull();
	    assertThat(pedidoEmitido.getId()).isNotNull();
	    assertThat(pedidoEmitido.getDataCriacao()).isNotNull();
	    assertThat(pedidoEmitido.getStatus()).isEqualTo(StatusPedido.CRIADO);
	    assertThat(pedidoEmitido.getTaxaFrete()).isEqualTo(restaurante.getTaxaFrete());
	    assertThat(pedidoEmitido.getValorTotal()).isNotNull();
	    assertThat(pedidoEmitido.getFormaPagamento()).isEqualTo(formaPagamento);
	    assertThat(pedidoEmitido.getEnderecoEntrega().getCidade()).isEqualTo(cidade);
	    assertThat(pedidoEmitido.getCliente()).isEqualTo(cliente);
	    assertThat(pedidoEmitido.getRestaurante()).isEqualTo(restaurante);
	    
	    pedidoEmitido.getItens().forEach(i -> {
	        assertThat(i.getPrecoUnitario()).isEqualTo(i.getProduto().getPreco());
	        assertThat(i.getPedido()).isEqualTo(pedidoEmitido);
	    });
	}
	
	@Test
	public void deveLancarException_QuandoFormaPagamentoNaoForAceitaPeloRestaurante() {
		// criando dados necessários
		Usuario cliente = new Usuario();
		cliente.setId(1L);
		
		FormaPagamento formaPagamento = new FormaPagamento();
		formaPagamento.setId(1L);
		formaPagamento.setDescricao("Cartão de crédito");
		
		Restaurante restaurante = new Restaurante();
		restaurante.setId(1L);
		restaurante.setTaxaFrete(new BigDecimal(10));
		
		Cidade cidade = new Cidade();
		cidade.setId(1L);
		
		Endereco endereco = new Endereco();
		endereco.setCidade(cidade);
		
		Produto produto = new Produto();
		produto.setId(1L);
		produto.setPreco(new BigDecimal(20));
		
		ItemPedido item = new ItemPedido();
		item.setProduto(produto);
		item.setQuantidade(1);
		
	    Pedido pedido = new Pedido();
	    pedido.setId(1L);
	    pedido.setRestaurante(restaurante);
	    pedido.setFormaPagamento(formaPagamento);
	    pedido.setCliente(cliente);
	    pedido.setEnderecoEntrega(endereco);
	    pedido.setItens(Arrays.asList(item));
	    pedido.setDataCriacao(OffsetDateTime.now());
	    
	    // configurando as dependências do serviço
	    when(cadastroRestaurante.buscarOuFalhar(restaurante.getId()))
        		.thenReturn(restaurante);

		when(cadastroFormaPagamento.buscarOuFalhar(formaPagamento.getId()))
		        .thenReturn(formaPagamento);
		
		when(cadastroCidade.buscarOuFalhar(cidade.getId()))
		        .thenReturn(cidade);
		
		when(cadastroUsuario.buscarOuFalhar(cliente.getId()))
		        .thenReturn(cliente);
		
		when(cadastroProduto.buscarOuFalhar(restaurante.getId(), produto.getId()))
		        .thenReturn(produto);
		
		when(pedidoRepository.save(pedido)).thenReturn(pedido);
		
		// fazendo o teste lançar exceção
//		assertThrows(NegocioException.class, () -> emissaoPedidoService.emitir(pedido));
		try {
			emissaoPedidoService.emitir(pedido);
			fail("Deveria ter lançado uma exceção");
		} catch (NegocioException e) {
			assertThat(e.getMessage()).isEqualTo("Forma de pagamento 'Cartão de crédito' não é aceita por esse restaurante.");
		}
	}
	
	@Test
	public void deveLancarExcecao_QuandoRestauranteNaoForEncontrado() {
	    Pedido pedido = new Pedido();
	    Restaurante restaurante = new Restaurante();
	    restaurante.setId(1L);
	    pedido.setRestaurante(restaurante);

	    when(cadastroRestaurante.buscarOuFalhar(1L)).thenThrow(RestauranteNaoEncontradoException.class);

	    assertThrows(RestauranteNaoEncontradoException.class, () -> emissaoPedidoService.emitir(pedido));
	}
	
	@Test
	public void deveLancarExcecao_QuandoFormaPagamentoNaoForEncontrada() {
	    // criando os dados de teste
	    Restaurante restaurante = new Restaurante();
	    restaurante.setId(1L);
	    restaurante.setTaxaFrete(new BigDecimal("10.00"));
	    
	    FormaPagamento formaPagamento = new FormaPagamento();
	    formaPagamento.setId(1L);
	    
	    restaurante.adicionarFormaPagamento(formaPagamento);
	    
	    Cidade cidade = new Cidade();
	    cidade.setId(1L);
	    
	    Usuario cliente = new Usuario();
	    cliente.setId(1L);
	    
	    Produto produto = new Produto();
	    produto.setId(1L);
	    produto.setPreco(new BigDecimal("100.00"));
	    
	    ItemPedido item = new ItemPedido();
	    item.setProduto(produto);
	    item.setQuantidade(1);
	    
	    Endereco endereco = new Endereco();
	    endereco.setCidade(cidade);
	    
	    Pedido pedido = new Pedido();
	    pedido.setId(1L);
	    pedido.setCodigo("24c6ec01-afb7-11ed-b94e-0242ac110002");
	    pedido.setRestaurante(restaurante);
	    pedido.setFormaPagamento(formaPagamento);
	    pedido.setCliente(cliente);
	    pedido.setEnderecoEntrega(endereco);
	    pedido.setItens(Arrays.asList(item));
	    pedido.setDataCriacao(OffsetDateTime.now());
	    
	    // Ação
	    doThrow(FormaPagamentoNaoEncontradaException.class)
	        .when(cadastroFormaPagamento)
	        .buscarOuFalhar(formaPagamento.getId());
	    
	    // Verificação
	    assertThrows(NegocioException.class, () -> emissaoPedidoService.emitir(pedido));
	}
	
}
