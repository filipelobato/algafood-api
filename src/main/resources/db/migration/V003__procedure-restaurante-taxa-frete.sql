DELIMITER //

CREATE PROCEDURE FILTRAR_RESTAURANTE_POR_TX_FRETE(in taxa_frete_inicial double)
BEGIN
	SELECT * FROM restaurante r WHERE r.taxa_frete >= taxa_frete_inicial ORDER BY taxa_frete;
END // 
DELIMITER;
