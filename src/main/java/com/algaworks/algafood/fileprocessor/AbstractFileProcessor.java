package com.algaworks.algafood.fileprocessor;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractFileProcessor {

	public abstract List<String[]> processFile(String filePath) throws IOException;

	protected List<String[]> readFile(String filePath) throws IOException {
		List<String[]> lines = new ArrayList<>();
		try (BufferedReader br = new BufferedReader(
				new InputStreamReader(new FileInputStream(filePath), StandardCharsets.ISO_8859_1))) {
			br.lines().forEach(line -> lines.add(line.split(";")));
		}
		return lines;
	}
}