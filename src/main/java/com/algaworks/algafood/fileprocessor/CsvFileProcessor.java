package com.algaworks.algafood.fileprocessor;

import java.io.IOException;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class CsvFileProcessor extends AbstractFileProcessor {

	@Override
	public List<String[]> processFile(String filePath) throws IOException {
		return readFile(filePath);
	}
}
