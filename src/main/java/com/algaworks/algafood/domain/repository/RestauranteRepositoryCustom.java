package com.algaworks.algafood.domain.repository;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.algaworks.algafood.domain.model.Restaurante;

public interface RestauranteRepositoryCustom {
	
	List<Restaurante> listar();
	
	Restaurante buscar(Long id);
	
	Restaurante salvar(Restaurante restaurante);
	
	void remover(Restaurante restaurante);

	List<Restaurante> listarRestaurantePorTaxaFreteProcedure(BigDecimal taxaFrete);

	Page<Restaurante> find(String nome, BigDecimal taxaFreteInicial, BigDecimal taxaFreteFinal, Pageable pageable);

	List<Restaurante> findWithCriteria(String nome, BigDecimal taxaFreteInicial, BigDecimal taxaFreteFinal);
	
	List<Restaurante> findComFreteGratis(String nome);
}
