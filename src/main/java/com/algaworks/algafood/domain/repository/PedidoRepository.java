package com.algaworks.algafood.domain.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.algaworks.algafood.domain.filter.VendaDiariaFilter;
import com.algaworks.algafood.domain.model.Pedido;
import com.algaworks.algafood.domain.model.dto.VendaDiaria;

@Repository
public interface PedidoRepository extends CustomJpaRepository<Pedido, Long>, JpaSpecificationExecutor<Pedido> {

	@Query("from Pedido p left join fetch p.restaurante r left join fetch p.cliente left join fetch r.cozinha")
	List<Pedido> findAll();

//	@Query("from Pedido p "
//			+ "left join fetch p.restaurante r "
//			+ "left join fetch p.cliente "
//			+ "left join fetch r.cozinha "
//			+ "join fetch p.formaPagamento "
//			+ "join fetch p.itens i "
//			+ "join fetch i.produto "
//			+ "left join fetch p.enderecoEntrega ee "
//			+ "join fetch ee.cidade c "
//			+ "join fetch c.estado "
//			+ "where p.codigo = :codigo")
	Optional<Pedido> findByCodigo(String codigo);
	
	Page<Pedido> findAll(Pageable pageable);	
	
	// 2ª alternativa para Estatisticas - Com DTO + JPQL
	@Query("select new com.algaworks.algafood.domain.model.dto.VendaDiaria(date(dataCriacao), "
			+ "count(id), sum(valorTotal)) from Pedido group by date(dataCriacao)")
	List<VendaDiaria> consultarVendasDiariasJPQL(VendaDiariaFilter filtro);
	
}
