package com.algaworks.algafood.domain.repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.algaworks.algafood.domain.model.Restaurante;

public interface RestauranteRepository 
	extends CustomJpaRepository<Restaurante, Long>, RestauranteRepositoryCustom,
	JpaSpecificationExecutor<Restaurante> {
	
	@Query("select distinct r from Restaurante r join fetch r.cozinha left join fetch r.formasPagamento "
			+ "order by r.id")
	List<Restaurante> findAll();
//	Page<Restaurante> findAll(Pageable pageable);
	
	
	// prefixos query methods: query, stream, read, get...
	List<Restaurante> queryByNomeContainingAndCozinhaId(String nome, Long cozinha);
	
	@Query("from Restaurante where nome like %:nome% and cozinha.id = :id")
	List<Restaurante> consultarPorNome(@Param("nome") String nome, @Param("id") Long cozinha);
	
	List<Restaurante> findByTaxaFreteBetween(BigDecimal taxaInicial, BigDecimal taxaFinal);
	
	Optional<Restaurante> findFirstRestauranteByNomeContaining(String nome);
	
	List<Restaurante> findTop2ByNomeContainingOrderByNomeAsc(String nome);
	
	int countByCozinhaId(Long cozinhaId);
	
	@Query("select count(*) from Restaurante where cozinha.id = :id")
	int contarOcorrencias(@Param("id") Long cozinhaId);
	
	@Query(nativeQuery = true, value = "call FILTRAR_RESTAURANTE_POR_TX_FRETE(:taxa_frete_inicial)")
	List<Restaurante> filtrarPorTaxaFreteNativeQuery(@Param("taxa_frete_inicial") BigDecimal taxa_frete_inicial);
	
	List<Restaurante> consultarPorNomeNamedQuery(@Param("nome") String nome, @Param("id") Long cozinha);
	
	
	@Query("select distinct r from Restaurante r join fetch r.formasPagamento "
			+ "where r.id = :id order by r.id")
	Restaurante buscarRestauranteComFormasPagamento(Long id);
	
}
