package com.algaworks.algafood.domain.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * Não deve ser levada em conta para fins de instanciação  de repositório
 * pelo Spring Data JPA
 * @author FILIPE
 */
@NoRepositoryBean
public interface CustomJpaRepository<T, ID> extends JpaRepository<T, ID> {

	Optional<T> buscarPrimeiro();

	T findOrFail(Long id);
	
	void detach(T entity);
	
}
