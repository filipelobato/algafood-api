package com.algaworks.algafood.domain.model.dto;

import java.math.BigDecimal;
import java.util.Date;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter 
@AllArgsConstructor
public class VendaDiaria {
	
	@ApiModelProperty(example = "2019-11-02 20:34:04-03:00")
	private Date data;
	
	@ApiModelProperty(example = "2")
	private Long totalVendas;
	
	@ApiModelProperty(example = "276.60")
	private BigDecimal totalFaturado;
}
