package com.algaworks.algafood.domain.model;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.groups.ConvertGroup;
import javax.validation.groups.Default;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.algaworks.algafood.core.validation.Groups;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

//@ValorZeroIncluiDescricao(valorField = "taxaFrete", 
//	descricaoField = "nome", descricaoObrigatoria = "Frete Grátis")
@Entity
@Table
@Getter @Setter @EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor @AllArgsConstructor
public class Restaurante {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	//@Setter(value = AccessLevel.NONE)
	@EqualsAndHashCode.Include
	private Long id;

	@NotBlank(message = "O nome é obrigatório")
	@Column(nullable = false)
	private String nome;

//	@NotNull
//	@DecimalMin(value = "0.01")
//	@Multiplo(numero = 5)
	@Column(name = "taxa_frete", nullable = false)
	private BigDecimal taxaFrete;

	@Valid
	@ConvertGroup(from = Default.class, to = Groups.CozinhaId.class)
	@ManyToOne
//	@NotNull
	@JoinColumn(name = "cozinha_id", nullable = false)
	private Cozinha cozinha;	
	
	@ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
    		name="restaurante_forma_pagamento", 
            joinColumns=
                @JoinColumn(name="restaurante_id", referencedColumnName="id"),
            inverseJoinColumns=
                @JoinColumn(name="forma_pagamento_id", referencedColumnName="id")
    		, uniqueConstraints = @UniqueConstraint(columnNames={"restaurante_id", "forma_pagamento_id"})
    		)
	private Set<FormaPagamento> formasPagamento = new HashSet<>();
	
	@Embedded	
	private Endereco endereco;
	
	private Boolean ativo = Boolean.TRUE;
	
	private Boolean aberto = Boolean.FALSE;
	
	@CreationTimestamp
	@Column(nullable = false, columnDefinition = "datetime", updatable = false)
	private OffsetDateTime dataCadastro;
	
	@UpdateTimestamp
	@Column(nullable = false, columnDefinition = "datetime")
	private OffsetDateTime dataAtualizacao;
	
	@OneToMany(mappedBy = "restaurante", fetch = FetchType.LAZY)
	private List<Produto> produtos = new ArrayList<>();
	
	@ManyToMany
    @JoinTable(
    		name="restaurante_usuario_responsavel", 
            joinColumns=
                @JoinColumn(name="restaurante_id", referencedColumnName="id"),
            inverseJoinColumns=
                @JoinColumn(name="usuario_id", referencedColumnName="id"))
	private Set<Usuario> responsaveis = new HashSet<>();
	
	public void ativar() {
		setAtivo(true);
	}
	
	public void inativar() {
		setAtivo(false);
	}
	
	public boolean removerFormaPagamento(FormaPagamento formaPagamento) {
		return getFormasPagamento().remove(formaPagamento);
	}
	
	public boolean adicionarFormaPagamento(FormaPagamento formaPagamento) {
		return getFormasPagamento().add(formaPagamento);
	}

	public boolean aceitaFormaPagamento(FormaPagamento formaPagamento) {
		return getFormasPagamento().contains(formaPagamento);
	}
	
	public boolean naoAceitaFormaPagamento(FormaPagamento formaPagamento) {
		return !aceitaFormaPagamento(formaPagamento);
	}
	
	public void abrir() {
		setAberto(true);
	}
	
	public void fechar() {
		setAberto(false);
	}
	
	public boolean removerResponsavel(Usuario responsavel) {
		return this.responsaveis.remove(responsavel);
	}
	
	public boolean adicionarResponsavel(Usuario responsavel) {
		return this.responsaveis.add(responsavel);
	}
	
	public boolean isAberto() {
		return this.aberto;
	}
	
	public boolean isFechado() {
		return !isAberto();
	}
	
	public boolean isAtivo() {
		return this.ativo;
	}
	
	public boolean isInativo() {
		return !isAtivo();
	}
	
	public boolean aberturaPermitida() {
	    return isAtivo() && isFechado();
	}
	
	public boolean ativacaoPermitida() {
	    return isInativo();
	}
	
	public boolean inativacaoPermitida() {
	    return isAtivo();
	}
	
	public boolean fechamentoPermitido() {
		return isAberto();
	}
}