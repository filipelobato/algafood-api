package com.algaworks.algafood.domain.service;

import java.io.IOException;
import java.util.List;
import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.algaworks.algafood.domain.exception.NegocioException;
import com.algaworks.algafood.domain.model.FormaPagamento;
import com.algaworks.algafood.fileprocessor.AbstractFileProcessor;

@Service
public class ImportacaoArquivoService {
	
	private AbstractFileProcessor fileProcessor;
	
	@Autowired
	private CadastroFormaPagamentoService cadastroFormaPagamento;
	
	private final String path = "C:\\Users\\Filipe\\Documents\\";

	public ImportacaoArquivoService(
		AbstractFileProcessor fileProcessor, 
		CadastroFormaPagamentoService cadastroPagamento
	) {
		this.fileProcessor = fileProcessor;
		this.cadastroFormaPagamento = cadastroPagamento;
	}
	
	@Transactional
	public void importarDados(String fileName) {
		String caminhoArquivo = path + fileName;
		try {
			List<String[]> lines = fileProcessor.processFile(caminhoArquivo);
			IntStream.range(0, lines.size()).forEach(index -> {
				carregarFormaPagamento(lines.get(index));
			});
		} catch (IOException e) {
			throw new NegocioException("Não foi possível fazer a carga de dados", e);
		}

	}
	
	private void carregarFormaPagamento(String[] coluna) {
		FormaPagamento formaPagamento = new FormaPagamento();

		String descricao = coluna[1];
		
		formaPagamento.setDescricao(descricao);
		
		cadastroFormaPagamento.salvar(formaPagamento);
	}

}
