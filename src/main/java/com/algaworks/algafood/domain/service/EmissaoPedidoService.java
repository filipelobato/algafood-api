package com.algaworks.algafood.domain.service;

import org.springframework.stereotype.Service;

import com.algaworks.algafood.domain.exception.NegocioException;
import com.algaworks.algafood.domain.exception.PedidoNaoEncontradoException;
import com.algaworks.algafood.domain.model.Cidade;
import com.algaworks.algafood.domain.model.FormaPagamento;
import com.algaworks.algafood.domain.model.Pedido;
import com.algaworks.algafood.domain.model.Produto;
import com.algaworks.algafood.domain.model.Restaurante;
import com.algaworks.algafood.domain.model.Usuario;
import com.algaworks.algafood.domain.repository.PedidoRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class EmissaoPedidoService {
	
	private final PedidoRepository pedidoRepository;
	
	private final CadastroRestauranteService cadastroRestaurante;
	private final CadastroFormaPagamentoService cadastroFormaPagamento;
	private final CadastroCidadeService cadastroCidade;
	private final CadastroUsuarioService cadastroUsuario;
	private final CadastroProdutoService cadastroProduto;
	
	public Pedido buscarOuFalhar(String codigoPedido) {
		return pedidoRepository.findByCodigo(codigoPedido)
				.orElseThrow(() -> new PedidoNaoEncontradoException(codigoPedido));
	}
	
	public Pedido emitir(Pedido pedido) {
		validarPedido(pedido);
		validarItens(pedido);	
		
		pedido.setTaxaFrete(pedido.getRestaurante().getTaxaFrete());
		pedido.calcularValorTotal();
		
		return pedidoRepository.save(pedido);
	}
	
	private void validarPedido(Pedido pedido) {
		Long restauranteId = pedido.getRestaurante().getId();
		Restaurante restaurante = cadastroRestaurante.buscarOuFalhar(restauranteId);
		
		Long formaPagamentoId = pedido.getFormaPagamento().getId();
		FormaPagamento formaPagamento = cadastroFormaPagamento.buscarOuFalhar(formaPagamentoId);

		Long cidadeId = pedido.getEnderecoEntrega().getCidade().getId();
		Cidade cidade = cadastroCidade.buscarOuFalhar(cidadeId);
		
		Long clienteId = pedido.getCliente().getId();
		Usuario cliente = cadastroUsuario.buscarOuFalhar(clienteId);
		
		pedido.setFormaPagamento(formaPagamento);
		pedido.getEnderecoEntrega().setCidade(cidade);
		pedido.setRestaurante(restaurante);
		pedido.setCliente(cliente);
		
		if (restaurante.naoAceitaFormaPagamento(pedido.getFormaPagamento())) {
			throw new NegocioException(String.format("Forma de pagamento '%s' não é aceita por esse restaurante.", 
					pedido.getFormaPagamento().getDescricao()));
		}
	}
	
	private void validarItens(Pedido pedido) {
		pedido.getItens().forEach(item -> {
			Produto produto = cadastroProduto.buscarOuFalhar(
					pedido.getRestaurante().getId(), item.getProduto().getId());
			
			item.setProduto(produto);
			item.setPedido(pedido);
			item.setPrecoUnitario(produto.getPreco());
		});
	}
}
