package com.algaworks.algafood.domain.exception;

public class NomePessoaInvalidoException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
    public NomePessoaInvalidoException(String message) {
        super(message);
    }

    public NomePessoaInvalidoException(String message, Throwable cause) {
        super(message, cause);
    }
}
