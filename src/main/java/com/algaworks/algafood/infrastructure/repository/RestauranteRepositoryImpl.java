package com.algaworks.algafood.infrastructure.repository;

import static com.algaworks.algafood.infrastructure.repository.spec.RestauranteSpecs.comFreteGratis;
import static com.algaworks.algafood.infrastructure.repository.spec.RestauranteSpecs.comNomeContendo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.algaworks.algafood.domain.model.Restaurante;
import com.algaworks.algafood.domain.repository.RestauranteRepository;
import com.algaworks.algafood.domain.repository.RestauranteRepositoryCustom;

@Repository
public class RestauranteRepositoryImpl implements RestauranteRepositoryCustom {

	@PersistenceContext
	private EntityManager manager;
	
	// @Lazy evita referência circular
	@Autowired @Lazy
	private RestauranteRepository restauranteRepository;
	
	@Override
	public List<Restaurante> listar() {
		return manager.createQuery("from Restaurante", Restaurante.class).getResultList();
	}

	@Override
	public Restaurante buscar(Long id) {
		return manager.find(Restaurante.class, id);
	}

	@Override
	@Transactional
	public Restaurante salvar(Restaurante restaurante) {
		return manager.merge(restaurante);
	}

	@Override
	@Transactional
	public void remover(Restaurante restaurante) {
		restaurante = this.buscar(restaurante.getId());
		manager.remove(restaurante);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Restaurante> listarRestaurantePorTaxaFreteProcedure(BigDecimal taxaFrete) {
		String procedureName = "FILTRAR_RESTAURANTE_POR_TX_FRETE";
		StoredProcedureQuery query = manager
				.createStoredProcedureQuery(procedureName, Restaurante.class)
				.registerStoredProcedureParameter("taxa_frete_inicial", BigDecimal.class, ParameterMode.IN)
				.setParameter("taxa_frete_inicial", taxaFrete);
		return query.getResultList();
	}
	
	@Override
	public Page<Restaurante> find(String nome, BigDecimal taxaFreteInicial, BigDecimal taxaFreteFinal, Pageable pageable) {
		var jpql = new StringBuilder();
		jpql.append("from Restaurante r join fetch r.cozinha where 0 = 0 ");
		
		var jpqlCount = new StringBuilder();
		jpqlCount.append("from Restaurante where 0 = 0 ");
		
		var parametros = new HashMap<String, Object>();
		
		if (StringUtils.hasText(nome)) {
			jpql.append("and r.nome like :nome ");
			jpqlCount.append("and nome like :nome ");
			parametros.put("nome", "%" + nome + "%");
		}
		
		if (taxaFreteInicial != null) {
			jpql.append("and taxa_frete >= :taxaInicial ");
			jpqlCount.append("and taxa_frete >= :taxaInicial ");
			parametros.put("taxaInicial", taxaFreteInicial);
		}
		
		if (taxaFreteFinal != null) {
			jpql.append("and taxa_frete <= :taxaFinal");
			jpqlCount.append("and taxa_frete <= :taxaFinal");
			parametros.put("taxaFinal", taxaFreteFinal);
		}
		
		TypedQuery<Restaurante> query = manager.createQuery(jpql.toString(), Restaurante.class);
		
		parametros.forEach((key, value) -> query.setParameter(key, value));
		
		query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
		query.setMaxResults(pageable.getPageSize());
		
		var restaurantes = query.getResultList();

		var countQuery = manager.createQuery("select count(*) " + jpqlCount.toString(), Long.class);
		parametros.forEach((key, value) -> countQuery.setParameter(key, value));
		
		return new PageImpl<>(restaurantes, pageable, countQuery.getSingleResult());
	}
	
	@Override
	public List<Restaurante> findWithCriteria(String nome, BigDecimal taxaFreteInicial, BigDecimal taxaFreteFinal) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		
		CriteriaQuery<Restaurante> criteria = builder.createQuery(Restaurante.class);
		Root<Restaurante> root = criteria.from(Restaurante.class); // from Restaurante
		root.fetch("cozinha");
		root.fetch("responsaveis").fetch("grupos", JoinType.LEFT);
		
		var predicates = new ArrayList<Predicate>();
		
		if (StringUtils.hasText(nome)) {
			predicates.add(builder.like(root.get("nome"), "%" + nome + "%"));
		}
		
		if (taxaFreteInicial != null) {
			predicates.add(builder.greaterThanOrEqualTo(root.get("taxaFrete"), taxaFreteInicial));
		}
		
		if (taxaFreteFinal != null) {
			predicates.add(builder.lessThanOrEqualTo(root.get("taxaFrete"), taxaFreteFinal));
		}
		
		criteria.where(predicates.toArray(new Predicate[0]));
		
		TypedQuery<Restaurante> query = manager.createQuery(criteria);
		
		return query.getResultList();
	}
	
	@Override 
	public List<Restaurante> findComFreteGratis(String nome) {
		return restauranteRepository.findAll(comFreteGratis().and(comNomeContendo(nome)));
	}
	
}
