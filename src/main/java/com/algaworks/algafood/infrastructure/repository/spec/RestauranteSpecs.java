package com.algaworks.algafood.infrastructure.repository.spec;

import java.math.BigDecimal;

import org.springframework.data.jpa.domain.Specification;

import com.algaworks.algafood.api.utils.StringHelper;
import com.algaworks.algafood.domain.model.Restaurante;

public class RestauranteSpecs {

	public static Specification<Restaurante> comNomeContendo(String nome) {
		return (root, criteriaQuery, criteriaBuilder) -> 
				criteriaBuilder.like(root.get("nome"), 
						"%" + StringHelper.removerAcentuacoes(nome) + "%");
	}
	
	public static Specification<Restaurante> comTaxaFreteMinimaDe(BigDecimal taxaMinimaFrete) {
		return (root, criteriaQuery, criteriaBuilder) -> 
			taxaMinimaFrete == null ? null :
					criteriaBuilder.greaterThanOrEqualTo(root.get("taxaFrete"), taxaMinimaFrete);

	}
	
	public static Specification<Restaurante> comFreteGratis() {
		return (root, criteriaQuery, criteriaBuilder) ->
			criteriaBuilder.equal(root.get("taxaFrete"), BigDecimal.ZERO);

	}

}
