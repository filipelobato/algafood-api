package com.algaworks.algafood.infrastructure.repository;

import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import com.algaworks.algafood.domain.repository.CustomJpaRepository;

public class CustomJpaRepositoryImpl<T, ID> extends SimpleJpaRepository<T, ID> 
	implements CustomJpaRepository<T, ID> {

	private EntityManager manager;
	
	public CustomJpaRepositoryImpl(JpaEntityInformation<T, ?> entityInformation, 
			EntityManager entityManager
			) {
		super(entityInformation, entityManager);
		
		this.manager = entityManager;
	}

	@Override
	public Optional<T> buscarPrimeiro() {
		var jpql = "from " + getDomainClass().getName();
		
		T entity = manager.createQuery(jpql, getDomainClass())
			.setMaxResults(1)
			.getSingleResult();
		
		return Optional.ofNullable(entity);
	}
	
	@Override
	public T findOrFail(Long id) {
	    String jpql = String.format("from %s where id = :id", getDomainClass().getName());

	    try {
	        T entity = manager
	            .createQuery(jpql, getDomainClass())
	            .setParameter("id", id)
	            .getSingleResult();

	        return entity;
	    } catch (NoResultException e) {
	    	throw new ResponseStatusException(HttpStatus.NOT_FOUND, 
	    			String.format("Entidade %s com código %d não encontrada", 
	    					getDomainClass().getSimpleName().toLowerCase(), id));
//	        throw new EntidadeNaoEncontradaException(String.format("Entidade %s com código %d não encontrada", 
//	        		getDomainClass().getSimpleName().toLowerCase(), id));
	    }
	}

	@Override
	public void detach(T entity) {
		manager.detach(entity);
	}

}
