package com.algaworks.algafood.infrastructure.service.storage;

import java.net.URL;

import org.springframework.beans.factory.annotation.Autowired;

import com.algaworks.algafood.core.storage.StorageProperties;
import com.algaworks.algafood.domain.exception.FotoProdutoNaoEncontradaException;
import com.algaworks.algafood.domain.service.FotoStorageService;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.GetObjectMetadataRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;

public class S3FotoStorageService implements FotoStorageService {

	@Autowired
	private AmazonS3 amazonS3;
	
	@Autowired
	private StorageProperties storageProperties;
	
	
	@Override
	public FotoRecuperada recuperar(String nomeArquivo) {
		
		try {

			String caminhoArquivo = getCaminhoArquivo(nomeArquivo);
			
			GetObjectMetadataRequest getObjectMetadataRequest = new GetObjectMetadataRequest(
					storageProperties.getS3().getBucket(), 
					caminhoArquivo);
			amazonS3.getObjectMetadata(getObjectMetadataRequest);
			
			URL url = amazonS3.getUrl(storageProperties.getS3().getBucket(), caminhoArquivo);
			
			return FotoRecuperada.builder()
					.url(url.toString()).build();
		
		} catch (AmazonS3Exception e) {
			throw new FotoProdutoNaoEncontradaException("Foto não existe no repositório da Amazon");
		} catch (Exception e) {
			throw new StorageException("Não foi possível recuperar arquivo para Amazon S3.", e);
		}
	}
	
	
	// Este método gera uma URL com tempo para expiração da imagem
//	@Override
//	public FotoRecuperada recuperar(String nomeArquivo) {
//
//		String caminhoArquivo = getCaminhoArquivo(nomeArquivo);
//
//		URL url = null;
//
//		try {
//			
//			BasicAWSCredentials credentials = new BasicAWSCredentials(
//					storageProperties.getS3().getIdChaveAcesso(), 
//					storageProperties.getS3().getChaveAcessoSecreta());
//			
//			AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
//					.withRegion(storageProperties.getS3().getRegiao())
//					.withCredentials(new AWSStaticCredentialsProvider(credentials)).build();
//
//			// URL expirará depois de uma hora.
//			java.util.Date expiration = new java.util.Date();
//			long expTimeMillis = expiration.getTime();
//			expTimeMillis += 1000 * 60 * 60;
//			expiration.setTime(expTimeMillis);
//
//			// Gerando a URL.
//			GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(
//					storageProperties.getS3().getBucket(), caminhoArquivo)
//					.withMethod(HttpMethod.GET)
//					.withExpiration(expiration);
//
//			url = s3Client.generatePresignedUrl(generatePresignedUrlRequest);
//
//		} catch (AmazonServiceException e) {
//			e.printStackTrace();
//		} catch (SdkClientException e) {
//			e.printStackTrace();
//		}
//
//		return FotoRecuperada.builder().url(url.toString()).build();
//	}

	@Override
	public void armazenar(NovaFoto novaFoto) {
		try {
			String caminhoArquivo = getCaminhoArquivo(novaFoto.getNomeArquivo());

			var objectMetadata = new ObjectMetadata();
			objectMetadata.setContentType(novaFoto.getContentType());
			
			var putObjectRequest = new PutObjectRequest(
					storageProperties.getS3().getBucket(), 
					caminhoArquivo,
					novaFoto.getInputStream(), 
					objectMetadata)
				.withCannedAcl(CannedAccessControlList.PublicRead);
			
			amazonS3.putObject(putObjectRequest);
		} catch (Exception e) {
			throw new StorageException("Não foi possível enviar arquivo para Amazon S3.", e);
		}
	}

	private String getCaminhoArquivo(String nomeArquivo) {
		return String.format("%s/%s", storageProperties.getS3().getDiretorioFotos(), nomeArquivo);
	}

	@Override
	public void remover(String nomeArquivo) {
		try {
			String caminhoArquivo = getCaminhoArquivo(nomeArquivo);
			
			var deleteObjectRequest = new DeleteObjectRequest(
					storageProperties.getS3().getBucket(), 
					caminhoArquivo);
			
			amazonS3.deleteObject(deleteObjectRequest);
		} catch (Exception e) {
			throw new StorageException("Não foi possível remover arquivo na Amazon S3.", e);
		}
	}

}
