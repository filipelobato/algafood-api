package com.algaworks.algafood.infrastructure.service.query;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.Predicate;

import org.springframework.stereotype.Repository;

import com.algaworks.algafood.domain.filter.VendaDiariaFilter;
import com.algaworks.algafood.domain.model.Pedido;
import com.algaworks.algafood.domain.model.StatusPedido;
import com.algaworks.algafood.domain.model.dto.VendaDiaria;
import com.algaworks.algafood.domain.service.VendaQueryService;

@Repository
public class VendaQueryServiceImpl implements VendaQueryService {

	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public List<VendaDiaria> consultarVendasDiarias(VendaDiariaFilter filtro, String timeOffset) {
		var builder = manager.getCriteriaBuilder();
		// Tipo que a consulta retorna, não precisa ser Domain Model, pois não é cláusula from
		var query = builder.createQuery(VendaDiaria.class);
		var root = query.from(Pedido.class);
		
		// Pedido.Fields.dataCriacao -> usa @FieldNameConstants. Evita problemas em runtime
		var functionConvertTzDataCriacao = builder.function(
				"convert_tz", Date.class, root.get(Pedido.Fields.dataCriacao),
				builder.literal("+00:00"), builder.literal(timeOffset));
		
		var functionDateDataCriacao = builder.function("date", Date.class, functionConvertTzDataCriacao);
		
		var selection = builder.construct(VendaDiaria.class, 
				functionDateDataCriacao, builder.count(root.get("id")),
				builder.sum(root.get("valorTotal")));
		
		var predicates = new ArrayList<Predicate>();
		
		if (filtro.getRestauranteId() != null) {
			predicates.add(builder.equal(root.get("id"), filtro.getRestauranteId()));
		}
		
		if (filtro.getDataCriacaoInicio() != null) {
			predicates.add(builder.greaterThanOrEqualTo(root.get(Pedido.Fields.dataCriacao), 
					filtro.getDataCriacaoInicio().with(LocalTime.MIN)));
		}
		
		if (filtro.getDataCriacaoFim() != null) {
			predicates.add(builder.lessThanOrEqualTo(root.get("dataCriacao"), 
					filtro.getDataCriacaoFim().with(LocalTime.MAX)));
		}
		
		predicates.add(root.get("status").in(StatusPedido.CONFIRMADO, StatusPedido.ENTREGUE));
		
		query.select(selection);
		
		query.groupBy(functionDateDataCriacao);
		
		query.where(predicates.toArray(new Predicate[0]));
		
		return manager.createQuery(query).getResultList();
	}
	
	@Override
	public List<VendaDiaria> consultarVendasDiariasJPQL(VendaDiariaFilter filtro) {
		var jpql = new StringBuilder(
				"SELECT new com.algaworks.algafood.domain.model.dto.VendaDiaria(" +
				"date(p.dataCriacao), count(p.id), sum(p.valorTotal)) " +
				"FROM Pedido p WHERE 1 = 1 ");
		
		
		jpql.append(String.format("and status in ('%s', '%s') ", 
				StatusPedido.CONFIRMADO, StatusPedido.ENTREGUE));
		
		var parametros = new HashMap<String, Object>();
		
		if (filtro.getRestauranteId() != null) {
			jpql.append("and p.id = :id ");
			parametros.put("id", filtro.getRestauranteId());
		}
		
		if (filtro.getDataCriacaoInicio() != null) {
			jpql.append("and p.dataCriacao >= :dataCriacaoInicio ");
			parametros.put("dataCriacaoInicio", 
					filtro.getDataCriacaoInicio().with(LocalTime.MIN));
		}
		
		if (filtro.getDataCriacaoFim() != null) {
			jpql.append("and p.dataCriacao <= :dataCriacaoFim ");
			parametros.put("dataCriacaoFim", 
					filtro.getDataCriacaoFim().with(LocalTime.MAX.truncatedTo(ChronoUnit.SECONDS)));
		}	
		
		jpql.append("group by date(p.dataCriacao) ");
		
		// Coloca-se a classe resultando, no caso VendaDiaria
		TypedQuery<VendaDiaria> query = manager.createQuery(jpql.toString(), VendaDiaria.class);
		
		parametros.forEach((key, value) -> query.setParameter(key, value));
		
		return query.getResultList();
	}
	
}
