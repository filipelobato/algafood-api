package com.algaworks.algafood.infrastructure.service.email;

import org.springframework.beans.factory.annotation.Autowired;

import com.algaworks.algafood.core.email.EmailProperties;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
public class SandboxEnvioEmailService extends SmtpEnvioEmailService  {
	
	@Autowired
	private EmailProperties emailProperties;
	
	@Override
	public void enviar(Mensagem mensagem) {
		
		mensagem = Mensagem.builder()
				.destinatario(emailProperties.getSandbox().getDestinatario())
				.assunto(mensagem.getAssunto())
				.corpo(mensagem.getCorpo())
				.variaveis(mensagem.getVariaveis())
				.build();

		log.info("[SANDBOX E-MAIL] Para: {}\n", mensagem.getDestinatarios());
		
		super.enviar(mensagem);
	}

}