package com.algaworks.algafood.infrastructure.service.storage;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;

import com.algaworks.algafood.core.storage.StorageProperties;
import com.algaworks.algafood.domain.exception.FotoProdutoNaoEncontradaException;
import com.algaworks.algafood.domain.service.FotoStorageService;

import io.minio.GetPresignedObjectUrlArgs;
import io.minio.MinioClient;
import io.minio.PutObjectArgs;
import io.minio.RemoveObjectArgs;
import io.minio.StatObjectArgs;
import io.minio.http.Method;

public class MinIOLocalStorageService implements FotoStorageService {
    
	@Autowired
    private MinioClient minioClient;

    @Autowired
    private StorageProperties storageProperties;

    @Override
    public FotoRecuperada recuperar(String nomeArquivo) {
        try {
            String caminhoArquivo = getCaminhoArquivo(nomeArquivo);

            // Verifica se o arquivo existe
            minioClient.statObject(StatObjectArgs.builder()
                    .bucket(storageProperties.getMinio().getBucket())
                    .object(caminhoArquivo)
                    .build());

            // Gera uma URL temporária para o arquivo
            String url = minioClient.getPresignedObjectUrl(GetPresignedObjectUrlArgs.builder()
                    .method(Method.GET)
                    .bucket(storageProperties.getMinio().getBucket())
                    .object(caminhoArquivo)
                    .expiry(7, TimeUnit.DAYS) // Expira em 7 dias
                    .build());

            return FotoRecuperada.builder()
                    .url(url)
                    .build();

        } catch (Exception e) {
            throw new FotoProdutoNaoEncontradaException("Foto não encontrada no MinIO.");
        }
    }

    @Override
    public void armazenar(NovaFoto novaFoto) {
        try {
            String caminhoArquivo = getCaminhoArquivo(novaFoto.getNomeArquivo());

            Map<String, String> tagTemporario = new HashMap<>();
            tagTemporario.put("temporario", "true");
            
            minioClient.putObject(PutObjectArgs.builder()
                    .bucket(storageProperties.getMinio().getBucket())
                    .object(caminhoArquivo)
                    .tags(tagTemporario)
                    .stream(novaFoto.getInputStream(), -1, 10485760) // Tamanho máximo de 10MB
                    .contentType(novaFoto.getContentType())
                    .build());

        } catch (Exception e) {
            throw new StorageException("Não foi possível enviar arquivo para o MinIO.", e);
        }
    }

    @Override
    public void remover(String nomeArquivo) {
        try {
            String caminhoArquivo = getCaminhoArquivo(nomeArquivo);

            minioClient.removeObject(RemoveObjectArgs.builder()
                    .bucket(storageProperties.getMinio().getBucket())
                    .object(caminhoArquivo)
                    .build());

        } catch (Exception e) {
            throw new StorageException("Não foi possível remover arquivo no MinIO.", e);
        }
    }

    private String getCaminhoArquivo(String nomeArquivo) {
        return String.format("%s/%s", storageProperties.getMinio().getDiretorioFotos(), nomeArquivo);
    }
}
