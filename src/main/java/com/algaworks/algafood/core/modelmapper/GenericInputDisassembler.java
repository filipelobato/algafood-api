package com.algaworks.algafood.core.modelmapper;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class GenericInputDisassembler<T, S> {

    private ModelMapper modelMapper;

    public GenericInputDisassembler(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public S toDomain(T originInput, Class<S> type) {
        return modelMapper.map(originInput, type);
    }

    public void copyToDomainObject(T originInput, Object destination) {
        modelMapper.map(originInput, destination);
    }
}
