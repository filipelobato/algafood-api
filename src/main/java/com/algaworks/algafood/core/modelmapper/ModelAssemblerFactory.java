package com.algaworks.algafood.core.modelmapper;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ModelAssemblerFactory {

	@Autowired
	private ModelMapper modelMapper;

	private static final Map<Class<?>, GenericModelAssembler<?, ?>> CACHE = new ConcurrentHashMap<>(); 
	
	@SuppressWarnings("unchecked")
	public <M, E> GenericModelAssembler<M, E> get(Class<M> modelType) {
		
		if (CACHE.containsKey(modelType)) {
			return (GenericModelAssembler<M, E>) CACHE.get(modelType);
		}

		GenericModelAssembler<M, E> modelAssembler = new GenericModelAssembler<M, E>(modelMapper, modelType);

		CACHE.put(modelType, modelAssembler);

		return modelAssembler;
	}
	
	public static class GenericModelAssembler<M, E> {
		
		private ModelMapper modelMapper;
		
		private Class<M> modelType;
		
		private GenericModelAssembler(
				ModelMapper mapper, 
				Class<M> modelType) {
			this.modelMapper = mapper;
			this.modelType = modelType;
		}
		
		public M toModel(E entity) {
			return modelMapper.map(entity, this.modelType);
		}
		
		public List<M> toCollectionModel(List<E> entities) {
			
			return entities.stream()
					.map(entity -> toModel(entity))
					.collect(Collectors.toList());
		}
		
	}
}
