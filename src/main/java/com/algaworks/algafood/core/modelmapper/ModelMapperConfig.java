package com.algaworks.algafood.core.modelmapper;

import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.algaworks.algafood.api.model.EnderecoModel;
import com.algaworks.algafood.api.model.input.CidadeIdInput;
import com.algaworks.algafood.api.model.input.EstadoIdInput;
import com.algaworks.algafood.api.model.input.ItemPedidoInput;
import com.algaworks.algafood.domain.model.Cidade;
import com.algaworks.algafood.domain.model.Endereco;
import com.algaworks.algafood.domain.model.Estado;
import com.algaworks.algafood.domain.model.ItemPedido;

@Configuration
public class ModelMapperConfig {

	@Bean
	public ModelMapper modelMapper() {
		var modelMapper = new ModelMapper();
		/** Configura atribução de valor a propriedade de nome (tokens) diferentes */
//		modelMapper.createTypeMap(Restaurante.class, RestauranteModel.class)
//			.addMapping(Restaurante::getTaxaFrete, RestauranteModel::setPrecoFrete);

		/**
		 * Configurar algoritmo de matching para pular atribuição de valor a tokens que
		 * não sejam totalmente correspondentes. Ex: restauranteId > cozinhaId
		 */
//        modelMapper.createTypeMap(RestauranteModel.class, Restaurante.class)
//        .addMappings(mapper -> mapper.skip(Restaurante::setId));

		var enderecoToEnderecoModelTypeMap = modelMapper.createTypeMap(Endereco.class, EnderecoModel.class);

		modelMapper.createTypeMap(ItemPedidoInput.class, ItemPedido.class)
				.addMappings(mapper -> mapper.skip(ItemPedido::setId));

		// Faz um map de Estado dentro de Cidade, que está em Endereco do Restaurante
		enderecoToEnderecoModelTypeMap.<String>addMapping(
				enderecoSrc -> enderecoSrc.getCidade().getEstado().getNome(),
				(enderecoModelDest, value) -> enderecoModelDest.getCidade().setEstado(value));
		return modelMapper;
	}

	@Bean
	Mapper mapper() {

		final ModelMapper modelMapper = new ModelMapper();

		var enderecoToEnderecoDtoTypeMap = modelMapper.createTypeMap(Endereco.class, EnderecoModel.class);

		enderecoToEnderecoDtoTypeMap.<String>addMapping(
				enderecoSrc -> enderecoSrc.getCidade().getEstado().getNome(),
				(enderecoDtoDest, value) -> enderecoDtoDest.getCidade().setEstado(value));

		Converter<EstadoIdInput, Estado> estadoConverter = new AbstractConverter<>() {
			@Override
			protected Estado convert(EstadoIdInput source) {
				Estado estado = new Estado();
				estado.setId(source.getId());
				return estado;
			}

		};

		Converter<CidadeIdInput, Cidade> cidadeConverter = new AbstractConverter<>() {
			@Override
			protected Cidade convert(CidadeIdInput source) {
				Cidade cidade = new Cidade();
				cidade.setId(source.getId());
				return cidade;
			}
		};

		modelMapper.addConverter(estadoConverter, EstadoIdInput.class, Estado.class);
		modelMapper.addConverter(cidadeConverter, CidadeIdInput.class, Cidade.class);

		return new Mapper() {
			@Override
			public <D> D map(Object source, Class<D> destinationType) {
				return modelMapper.map(source, destinationType);
			}

			@Override
			public void map(Object source, Object destination) {
				modelMapper.map(source, destination);
			}
		};
	}
}
