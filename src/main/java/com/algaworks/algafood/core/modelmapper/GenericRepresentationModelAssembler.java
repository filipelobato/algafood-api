package com.algaworks.algafood.core.modelmapper;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;

public abstract class GenericRepresentationModelAssembler<M extends RepresentationModel<M>, D, C>
		extends RepresentationModelAssemblerSupport<D, M> {

	private final Mapper mapper;
	private Class<M> dtoRepresentationObject;
	private List<Link> linkList;

	@SuppressWarnings("unchecked")
	public GenericRepresentationModelAssembler(Mapper mapper, Class<C> controllerClass, Class<M> dtoClass) {
		super(controllerClass, dtoClass);

		ParameterizedType type = (ParameterizedType) getClass().getGenericSuperclass();
		this.mapper = mapper;
		this.dtoRepresentationObject = (Class<M>) type.getActualTypeArguments()[0];
	}

	@Override
	public M toModel(D entityObject) {
		M dtoObject = this.mapper.map(entityObject, this.dtoRepresentationObject);
		this.linkList = constructLinks(entityObject);
		
		if (!this.linkList.isEmpty()) {
			for (Link link : linkList) {
				dtoObject.add(link);
			}
		}
		return dtoObject;
	}

	@Override 
	public CollectionModel<M> toCollectionModel(Iterable<? extends D> listOfEntityObjects) {
		return super.toCollectionModel(listOfEntityObjects).add(constructCollectionLink());
	}

	public abstract List<Link> constructLinks(D entityObject);

	public abstract Link constructCollectionLink();
}