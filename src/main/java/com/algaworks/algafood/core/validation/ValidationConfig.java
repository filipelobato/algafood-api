package com.algaworks.algafood.core.validation;

import java.util.Locale;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.FixedLocaleResolver;

@Configuration
public class ValidationConfig {

	@Bean
	public LocalValidatorFactoryBean validator(MessageSource messageSource) {
		LocalValidatorFactoryBean bean = new LocalValidatorFactoryBean();
		// utilizar o messages.properties como ValidationFactoryBean (ValidationMessages.properties)
		bean.setValidationMessageSource(messageSource);
		return bean;
	}
	
	@Bean
	public LocaleResolver localeResolver() {
		LocaleResolver localeResolver = new FixedLocaleResolver(Locale.forLanguageTag("pt-BR"));
		return localeResolver;
	}

}
