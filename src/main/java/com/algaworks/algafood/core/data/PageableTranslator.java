package com.algaworks.algafood.core.data;

import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.algaworks.algafood.domain.exception.NegocioException;

public class PageableTranslator {

	public static Pageable translate(Pageable pageable, Map<String, String> fieldsMapping) {
		var orders = pageable.getSort().stream()
				.filter(order -> {
					if (!fieldsMapping.containsKey(order.getProperty())) {
						throw new NegocioException(String.format("A propriedade '%s' não existe. "
								+ "Corrija ou remova essa propriedade e tente novamente.", order.getProperty()));
					}
					return true;
				})
				.map(order -> new Sort.Order(order.getDirection(), fieldsMapping.get(order.getProperty())))
				.collect(Collectors.toList());

		return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by(orders));
	}
}
