package com.algaworks.algafood.core.openapi.springfox;

import java.io.File;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.net.URLStreamHandler;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.algaworks.algafood.api.exceptionhandler.Problem;
import com.algaworks.algafood.api.model.CozinhaModel;
import com.algaworks.algafood.api.model.PedidoResumoModel;
import com.algaworks.algafood.api.openapi.model.PageableModelOpenApi;
import com.algaworks.algafood.api.openapi.model.PagedModelOpenApi;
import com.fasterxml.classmate.TypeResolver;
import com.google.common.base.Predicates;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.AlternateTypeRule;
import springfox.documentation.schema.AlternateTypeRules;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@Import(BeanValidatorConfig.class)
public class SpringFoxConfig implements WebMvcConfigurer {

	@Autowired
	private TypeResolver typeResolver;
	
	@Bean
	public Docket apiDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				// builder para selecionar os endpoints que devem ser expostos 
				.select()
					.apis(RequestHandlerSelectors.basePackage("com.algaworks.algafood.api.controller"))
					.paths(PathSelectors.any())
					// A barra /* refere-se a qlqr coisa com um nível de profundidade e /** a qlqr nível de profundidade 
//					.paths(PathSelectors.ant("/restaurantes/**"))
					.paths(Predicates.not(PathSelectors.ant("/teste/**")))
					.build()
				.useDefaultResponseMessages(false)
				.globalResponseMessage(RequestMethod.GET, globalGetResponseMessages())
				.globalResponseMessage(RequestMethod.POST, globalPostPutResponseMessages())
				.globalResponseMessage(RequestMethod.PUT, globalPostPutResponseMessages())
				.globalResponseMessage(RequestMethod.DELETE, globalDeleteResponseMessages())
				.additionalModels(typeResolver.resolve(Problem.class))
				.ignoredParameterTypes(ServletWebRequest.class, 
						URL.class, URI.class, URLStreamHandler.class, Resource.class, File.class,
						InputStream.class)
				.directModelSubstitute(Pageable.class, PageableModelOpenApi.class)
//				.alternateTypeRules(AlternateTypeRules.newRule(
//						typeResolver.resolve(Page.class, CozinhaModel.class), 
//						CozinhasModelOpenApi.class))
				.alternateTypeRules(buildPageTypeRule(CozinhaModel.class))
				.alternateTypeRules(buildPageTypeRule(PedidoResumoModel.class))
				.apiInfo(apiInfo())
				.tags(tags()[0], tags());
	}
	
    private <T> AlternateTypeRule buildPageTypeRule(Class<T> classModel) {
        return AlternateTypeRules.newRule(
                typeResolver.resolve(Page.class, classModel), 
                typeResolver.resolve(PagedModelOpenApi.class, classModel)
        );
    }
    
	private List<ResponseMessage> globalPostPutResponseMessages() {
		return Arrays.asList(
				new ResponseMessageBuilder()
					.code(HttpStatus.INTERNAL_SERVER_ERROR.value())
					.message("Erro interno do servidor")
					.responseModel(new ModelRef("Problema"))
					.build(), 
		        new ResponseMessageBuilder()
	                .code(HttpStatus.BAD_REQUEST.value())
	                .message("Requisição inválida (erro do cliente)")
	                .responseModel(new ModelRef("Problema"))
	                .build(),
				new ResponseMessageBuilder()
					.code(HttpStatus.NOT_ACCEPTABLE.value())
					.message("Recurso não possui representação que poderia ser aceita pelo consumidor")
					.build(),
	            new ResponseMessageBuilder()
	                .code(HttpStatus.UNSUPPORTED_MEDIA_TYPE.value())
	                .message("Requisição recusada porque o corpo está em um formato não suportado")
	                .responseModel(new ModelRef("Problema"))
	                .build()
			);
	}
	
	private List<ResponseMessage> globalGetResponseMessages() {
		return Arrays.asList(
				new ResponseMessageBuilder()
					.code(HttpStatus.INTERNAL_SERVER_ERROR.value())
					.message("Erro interno do servidor")
					.responseModel(new ModelRef("Problema"))
					.build(), 
				new ResponseMessageBuilder()
					.code(HttpStatus.NOT_ACCEPTABLE.value())
					.message("Recurso não possui representação que poderia ser aceita pelo consumidor")
					.build()
		);
	}
	
	private List<ResponseMessage> globalDeleteResponseMessages() {
		return Arrays.asList(
			new ResponseMessageBuilder()
				.code(HttpStatus.INTERNAL_SERVER_ERROR.value())
				.message("Erro interno do servidor")
				.responseModel(new ModelRef("Problema"))
				.build(), 
			new ResponseMessageBuilder()
				.code(HttpStatus.NOT_ACCEPTABLE.value())
				.message("Recurso não possui representação que poderia ser aceita pelo consumidor")
				.build(),
	        new ResponseMessageBuilder()
                .code(HttpStatus.NOT_FOUND.value())
                .message("Recurso não encontrado")
                .responseModel(new ModelRef("Problema"))
                .build()
		);
	}
	
	public ApiInfo apiInfo() {
		return new ApiInfoBuilder()
				.title("AlgaFood API")
				.description("API aberta para clientes e restaurantes")
				.version("1")
				.contact(new Contact("AlgaWorks", "https://www.algaworks.com", "contato@algaworks.com"))
				.build();
	}
	
	// No SpringFox 3.0, não é mais necessário configurar WebMvcConfigurer
	@Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
//        registry.addResourceHandler("swagger-ui.html")
//                .addResourceLocations("classpath:/META-INF/resources/");
//
//        registry.addResourceHandler("/webjars/**")
//                .addResourceLocations("classpath:/META-INF/resources/webjars/");
		
		// Config para arquivos Swagger personalizado 
        registry.addResourceHandler("/swagger-ui.html")
			.addResourceLocations("classpath:/META-INF/dist/resources/");

    	registry.addResourceHandler("/webjars/**")
			.addResourceLocations("classpath:/META-INF/dist/resources/webjars/");
    }
	
	private Tag[] tags() {
	    return new Tag[] {
	        new Tag("Cidades", "Gerencia as cidades"),
	        new Tag("Grupos", "Gerencia os grupos"),
	        new Tag("Cozinhas", "Gerencia as cozinhas"),
	        new Tag("Formas de Pagamento", "Gerencia as formas de pagamento"),
	        new Tag("Pedidos", "Gerencia os pedidos"),
	        new Tag("Restaurantes", "Gerencia os restaurantes"),
	        new Tag("Estados", "Gerencia os estados"),
	        new Tag("Produtos", "Gerencia os produtos de restaurantes"),
	        new Tag("Usuários", "Gerencia os usuários"),
	        new Tag("Estatísticas", "Gerencia as estatísticas"),
	        new Tag("Status do Pedido", "Gerencia os status dos pedidos"),
	        new Tag("Fotos do Produto", "Gerencia as fotos dos produtos"),
	        new Tag("Restaurantes", "Gerencia os restaurantes"),
	        new Tag("Formas de Pagamento do Restaurante", "Gerencia as formas de pagamento dos restaurantes"),
	        new Tag("Produtos do Restaurante", "Gerencia os produtos dos restaurantes"),
	        new Tag("Usuários do Restaurante", "Gerencia os usuários dos restaurantes"),
	        new Tag("Grupos de Usuários", "Gerencia os grupos de usuários")
	    };
	}
	
}
