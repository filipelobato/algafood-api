package com.algaworks.algafood.core.openapi.springfox;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;

@Configuration
public class BeanValidatorConfig extends BeanValidatorPluginsConfiguration {
	
	@Bean
	public NotBlankAnnotationPlugin notNullAnnotationPlugin() {
		return new NotBlankAnnotationPlugin();
	}
}
