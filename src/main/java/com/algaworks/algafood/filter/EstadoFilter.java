package com.algaworks.algafood.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class EstadoFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        System.out.printf("Logging Request  %s : %s \n", req.getMethod(), req.getRequestURI());
        chain.doFilter(request, response);
        System.out.println("Logging Response " + res.getContentType() + " : HttpStatus: " + String.valueOf(res.getStatus()));
	}
	
	@Bean
	public FilterRegistrationBean<EstadoFilter> loggingFilter(){
	    FilterRegistrationBean<EstadoFilter> registrationBean 
	      = new FilterRegistrationBean<>();
	        
	    registrationBean.setFilter(new EstadoFilter());
	    registrationBean.addUrlPatterns("/estados");
	        
	    return registrationBean;    
	}

}
