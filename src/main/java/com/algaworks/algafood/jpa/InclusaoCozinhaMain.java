package com.algaworks.algafood.jpa;

import java.util.Arrays;
import java.util.List;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;

import com.algaworks.algafood.AlgafoodApiApplication;
import com.algaworks.algafood.domain.model.Cozinha;
import com.algaworks.algafood.domain.repository.CozinhaRepository;

public class InclusaoCozinhaMain {
	public static void main(String[] args) {
		ApplicationContext applicationContext = new SpringApplicationBuilder(AlgafoodApiApplication.class)
				.web(WebApplicationType.NONE)
				.run(args);
		
		CozinhaRepository cadastroCozinha = applicationContext.getBean(CozinhaRepository.class);
		
		Cozinha cozinha1 = new Cozinha();
		cozinha1.setNome("Japa");
		
		Cozinha cozinha2 = new Cozinha();
		cozinha2.setNome("Brasa");
		
		List<Cozinha> cozinhas = Arrays.asList(cozinha1, cozinha2);
		//cozinhas.add(new Cozinha(3L, "teste"));
		
		cozinhas.forEach(c -> cadastroCozinha.save(c));
	}
}
