package com.algaworks.algafood.jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;

import com.algaworks.algafood.AlgafoodApiApplication;
import com.algaworks.algafood.domain.model.Cozinha;

public class PersistenciaEntityManagerMain {
	public static void main(String[] args) {
		ApplicationContext applicationContext = new SpringApplicationBuilder(AlgafoodApiApplication.class)
				.web(WebApplicationType.NONE)
				.run(args);

		EntityManagerFactory emf = applicationContext.getBean(EntityManagerFactory.class);
		EntityManager em = emf.createEntityManager();
		
		Cozinha cozinhaCriada = new Cozinha();
		cozinhaCriada.setId(9L);
		cozinhaCriada.setNome("Teste 87898 --66");
		
		em.getTransaction().begin();
		cozinhaCriada = em.merge(cozinhaCriada);
		em.getTransaction().commit();
		em.close();
		
		System.out.println(cozinhaCriada);

	}
}
