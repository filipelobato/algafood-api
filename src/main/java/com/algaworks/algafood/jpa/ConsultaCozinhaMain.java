package com.algaworks.algafood.jpa;

import java.util.List;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;

import com.algaworks.algafood.AlgafoodApiApplication;
import com.algaworks.algafood.domain.model.Cozinha;
import com.algaworks.algafood.domain.repository.CozinhaRepository;

/*
 * Classe criada para que executar os testes de integração com o banco de dados
 * sem a necessidade de subir o servidor web
 */
public class ConsultaCozinhaMain {
	
	public static void main(String[] args) {
		// ApplicationContext (Gerencia o contexto da aplicação) 
		// SpringApplicationBuilder (Padrão de construtor - Builder - de Spring Application)
		ApplicationContext applicationContext = new SpringApplicationBuilder(AlgafoodApiApplication.class)
				// Não é aplicação web
				.web(WebApplicationType.NONE)
				//Argumentos do mêtodo main (args)
				.run(args);
		
		CozinhaRepository cadastroCozinha = applicationContext.getBean(CozinhaRepository.class);
		
		List<Cozinha> cozinhas = cadastroCozinha.findAll();
		
		cozinhas.forEach(c -> System.out.println(c.getNome()));
	}
}
