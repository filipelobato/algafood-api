package com.algaworks.algafood.api.controller;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.util.ReflectionUtils;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.SmartValidator;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.algaworks.algafood.api.assembler.RestauranteApenasNomeModelAssembler;
import com.algaworks.algafood.api.assembler.RestauranteBasicoModelAssembler;
import com.algaworks.algafood.api.assembler.RestauranteModelAssembler;
import com.algaworks.algafood.api.disassembler.RestauranteInputDisassembler;
import com.algaworks.algafood.api.model.RestauranteApenasNomeModel;
import com.algaworks.algafood.api.model.RestauranteBasicoModel;
import com.algaworks.algafood.api.model.RestauranteModel;
import com.algaworks.algafood.api.model.input.RestauranteInput;
import com.algaworks.algafood.api.model.view.RestauranteView;
import com.algaworks.algafood.api.openapi.controller.RestauranteControllerOpenApi;
import com.algaworks.algafood.core.modelmapper.GenericModelAssembler;
import com.algaworks.algafood.core.validation.ValidacaoException;
import com.algaworks.algafood.domain.exception.CidadeNaoEncontradaException;
import com.algaworks.algafood.domain.exception.CozinhaNaoEncontradaException;
import com.algaworks.algafood.domain.exception.NegocioException;
import com.algaworks.algafood.domain.exception.RestauranteNaoEncontradoException;
import com.algaworks.algafood.domain.model.Restaurante;
import com.algaworks.algafood.domain.repository.RestauranteRepository;
import com.algaworks.algafood.domain.service.CadastroRestauranteService;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

/** Sem @CrossOrigin, mesmo dando problema de CORS, é feito o select. 
 * pois o select é feito no servidor e a política de CORS é feita somente no browser */
@CrossOrigin(maxAge = 10)
@RestController
@RequestMapping(path = "/restaurantes", produces = MediaType.APPLICATION_JSON_VALUE)
public class RestauranteController implements RestauranteControllerOpenApi {
	
	@Autowired
    private RestauranteRepository restauranteRepository;
	
	@Autowired
	private CadastroRestauranteService cadastroRestaurante;
	
	@Autowired
	private RestauranteModelAssembler restauranteModelAssembler;
	
	@Autowired
	private RestauranteInputDisassembler restauranteInputDisassembler;
	
	@Autowired
	private GenericModelAssembler<Restaurante, RestauranteInput> genericInput;
	
	@Autowired
	private SmartValidator validator;
	
	@Autowired
	private RestauranteApenasNomeModelAssembler restauranteApenasNomeAssembler;
	
	@Autowired
	private RestauranteBasicoModelAssembler restauranteBasicoModelAssembler;
	
//	@GetMapping
//    public MappingJacksonValue listar(@RequestParam(required = false) String projecao) {
//    	List<Restaurante> restaurantes = restauranteRepository.findAll();
//    	List<RestauranteModel> restauranteModel = modelAssemblerFactory.<RestauranteModel, Restaurante>get(RestauranteModel.class)
//    			.toCollectionModel(restaurantes);
//    	
//    	MappingJacksonValue restaurantesWrapper = new MappingJacksonValue(restauranteModel);
//    	restaurantesWrapper.setSerializationView(RestauranteView.Resumo.class);
//    	
//    	if ("apenas-nome".equals(projecao)) {
//    		restaurantesWrapper.setSerializationView(RestauranteView.ApenasNome.class);
//    	} else if ("completo".equals(projecao)) {
//    		restaurantesWrapper.setSerializationView(null);
//    	}
//    	
//    	return restaurantesWrapper;
//	}
	
//	@JsonView(RestauranteView.Resumo.class)
	@GetMapping
    public CollectionModel<RestauranteBasicoModel> listar() {
//    	System.out.println("O nome da cozinha é: terewr11111 ");
//    	System.out.println(restaurantes.get(0).getCozinha().getNome());
//    	System.out.println(restaurantes.get(0).getFormasPagamento().get(0).getDescricao());
//    	System.out.println(restaurantes.get(0).getProdutos().get(0).getDescricao());
    	
//    	var restauranteTeste = new ArrayList<Restaurante>();
//    	restauranteTeste.add(new Restaurante());
//    	restauranteTeste.get(0).getFormasPagamento().add(new FormaPagamento(1L, "Teste"));
//    	System.out.println(restauranteTeste.get(0).getFormasPagamento().get(0).getDescricao());
		
    	return restauranteBasicoModelAssembler.toCollectionModel(restauranteRepository.findAll());
	}

/** Testes de Cache no Cliente */
//	@GetMapping
//	@JsonView(RestauranteView.Resumo.class)
//	public ResponseEntity<List<RestauranteModel>> listar() {
//		return ResponseEntity.ok()
//				.cacheControl(CacheControl.maxAge(30, TimeUnit.SECONDS))
//				.body(restauranteModelAssembler
//				.toCollectionModel(restauranteRepository.findAll()));
//	}
	
//	@GetMapping
//	@JsonView(RestauranteView.Resumo.class)
//	public ResponseEntity<List<RestauranteModel>> listar() {
//		List<RestauranteModel> restaurantesModel = restauranteModelAssembler
//				.toCollectionModel(restauranteRepository.findAll());
//		
//		return ResponseEntity.ok()
//				.header(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, "http://www.algafood.local:8000")
//				.body(restaurantesModel);
//	}
	
//	@JsonView(RestauranteView.ApenasNome.class)
    @GetMapping(params="projecao=apenas-nome")
    public CollectionModel<RestauranteApenasNomeModel> listarApenasNomes() {
    	return restauranteApenasNomeAssembler.toCollectionModel(restauranteRepository.findAll());
	}
    
    @GetMapping("/{restauranteId}")
	public RestauranteModel buscar(@PathVariable Long restauranteId) {
		Restaurante restaurante = cadastroRestaurante.buscarOuFalhar(restauranteId);
		
		return restauranteModelAssembler.toModel(restaurante);
	}
    
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public RestauranteModel adicionar(@RequestBody @Valid RestauranteInput restauranteInput) {
    	try {
    		Restaurante restaurante = restauranteInputDisassembler.toDomainObject(restauranteInput);
    		
    		return restauranteModelAssembler.toModel(cadastroRestaurante.salvar(restaurante));
		} catch (CozinhaNaoEncontradaException | CidadeNaoEncontradaException e) {
			throw new NegocioException(e.getMessage());
		}
    }
    
    @PutMapping("/{restauranteId}")
    public RestauranteModel atualizar(@PathVariable Long restauranteId, 
    		@RequestBody @Valid RestauranteInput restauranteInput) {

    	Restaurante restauranteAtual = cadastroRestaurante.buscarOuFalhar(restauranteId);
    	try {
    		restauranteInputDisassembler.copyToDomainObject(restauranteInput, restauranteAtual);
        	
    		return restauranteModelAssembler.toModel(cadastroRestaurante.salvar(restauranteAtual));
		} catch (CozinhaNaoEncontradaException | CidadeNaoEncontradaException e) {
			throw new NegocioException(e.getMessage());
		}
    }
    
	@PatchMapping("/{restauranteId}")
	public RestauranteModel atualizarParcial(@PathVariable Long restauranteId, 
			@RequestBody HashMap<String, Object> campos, HttpServletRequest request) {
		Restaurante restauranteAtual = cadastroRestaurante.buscarOuFalhar(restauranteId);
		
		merge(campos, restauranteAtual, request);
		//ObjectMerger.mergeRequestBodyToGenericObject(campos, restauranteAtual, Restaurante.class);
		
		RestauranteInput restauranteInput = genericInput
				.toModel(restauranteAtual, RestauranteInput.class);
		
		validate(restauranteInput, "restaurante");

		try {
//			return null;
//			return atualizar(restauranteId, toModel(restauranteAtual));
			return atualizar(restauranteId, restauranteInput);
		} catch (CozinhaNaoEncontradaException e) {
			throw new NegocioException(e.getMessage());
		}
	}
	
	@PutMapping("/{restauranteId}/ativo")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public ResponseEntity<Void> ativar(@PathVariable Long restauranteId) {
		cadastroRestaurante.ativar(restauranteId);
		return ResponseEntity.noContent().build();
	}
	
	@DeleteMapping("/{restauranteId}/ativo")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public ResponseEntity<Void> inativar(@PathVariable Long restauranteId) {
		cadastroRestaurante.inativar(restauranteId);
		return ResponseEntity.noContent().build();
	}
	
	@PutMapping("/ativacoes") 
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void ativarMultiplos(@RequestBody List<Long> restauranteIds) {
		try {
			cadastroRestaurante.ativar(restauranteIds);
		} catch (RestauranteNaoEncontradoException e) {
			throw new NegocioException(e.getMessage(), e);
		}
	}
	
	@DeleteMapping("/ativacoes") 
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void inativarMultiplos(@RequestBody List<Long> restauranteIds) {
		try {
			cadastroRestaurante.inativar(restauranteIds);
		} catch (RestauranteNaoEncontradoException e) {
			throw new NegocioException(e.getMessage(), e);
		}
	}
	
	@PutMapping("/{restauranteId}/abertura")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public ResponseEntity<Void> abrir(@PathVariable Long restauranteId) {
		cadastroRestaurante.abrir(restauranteId);
		return ResponseEntity.noContent().build();
	}
	
	@PutMapping("/{restauranteId}/fechamento")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public ResponseEntity<Void> fechar(@PathVariable Long restauranteId) {
		cadastroRestaurante.fechar(restauranteId);
		return ResponseEntity.noContent().build();
	}
	
	private void validate(RestauranteInput restaurante, String objectName) {
		BeanPropertyBindingResult bindingResult = new BeanPropertyBindingResult(restaurante, objectName);
		validator.validate(restaurante, bindingResult);
		// validator.validateValue(Restaurante.class, "taxaFrete", restaurante.getTaxaFrete(), bindingResult);
		
		if (bindingResult.hasErrors()) {
			throw new ValidacaoException(bindingResult);
		}
		
	}

	private void merge(Map<String, Object> dadosOrigem, Restaurante restauranteDestino, HttpServletRequest request) {
		ServletServerHttpRequest servletHttpRequest = new ServletServerHttpRequest(request);
		
		try {
			
			// from Jackson
			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, true);
			objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, true);
			
			Restaurante restauranteOrigem = objectMapper.convertValue(dadosOrigem, Restaurante.class);
			
			dadosOrigem.forEach((key, value) -> {
				Field field = ReflectionUtils.findField(Restaurante.class, key);
				field.setAccessible(true);
				
				Object newValue = ReflectionUtils.getField(field, restauranteOrigem);
				
				ReflectionUtils.setField(field, restauranteDestino, newValue);
			});
		} catch (IllegalArgumentException e) {
			Throwable rootCause = ExceptionUtils.getRootCause(e);
			throw new HttpMessageNotReadableException(e.getMessage(), rootCause, servletHttpRequest);
		}
	}

}
