package com.algaworks.algafood.api.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.algaworks.algafood.api.model.PageResponse;
import com.algaworks.algafood.api.model.RestauranteResumoModel;
import com.algaworks.algafood.core.modelmapper.GenericModelAssembler;
import com.algaworks.algafood.domain.model.Cozinha;
import com.algaworks.algafood.domain.model.Restaurante;
import com.algaworks.algafood.domain.repository.CozinhaRepository;
import com.algaworks.algafood.domain.repository.RestauranteRepository;
import com.algaworks.algafood.domain.service.ImportacaoArquivoService;
import com.algaworks.algafood.infrastructure.repository.spec.RestauranteComNomeSemelhanteSpec;

@RestController
@RequestMapping("/teste")
public class TesteController {
    private static int varCompartilhada = 0;
    private static final Integer QUANTIDADE = 10000;
    private static final List<Integer> VALORES = new ArrayList<>();
    
    @Autowired
    private GenericModelAssembler<Restaurante, RestauranteResumoModel> restauranteAssembler;
	
	@Autowired
	private CozinhaRepository cozinhas;
	
	@Autowired
	private RestauranteRepository restaurantes;
	
	@Autowired
	private ImportacaoArquivoService carregamentoArquivoService;
	
	@GetMapping("/cozinhas/por-nome")
	public List<Cozinha> cozinhasPorNome(@RequestParam String nome) {
		return cozinhas.findByNomeContaining(nome);
	}
	
	@GetMapping("/cozinhas/unica-por-nome")
	public Optional<Cozinha> cozinhaUnicaPorNome(String nome) {
		return cozinhas.findByNome(nome);
	}
	
	@GetMapping("/cozinhas/exists")
	public boolean cozinhasExists(String nome) {
		return cozinhas.existsByNome(nome);
	}
	
	@GetMapping("/restaurantes/por-taxa-frete")
	public List<Restaurante> restaurantesPorTaxaFrete(
			BigDecimal taxaInicial, BigDecimal taxaFinal) {
		return restaurantes.findByTaxaFreteBetween(taxaInicial, taxaFinal);
	}
	
	@GetMapping("/restaurantes/por-nome")
	public List<Restaurante> restaurantesPorTaxaFrete(
			String nome, Long cozinhaId) {
		//return restaurantes.queryByNomeContainingAndCozinhaId(nome, cozinhaId);
		return restaurantes.consultarPorNome(nome, cozinhaId);
		
	}
	
	@GetMapping("/restaurantes/primeiro-por-nome")
	public Optional<Restaurante> restaurantePorTaxaFrete(String nome) {
		return restaurantes.findFirstRestauranteByNomeContaining(nome);
	}
	
	@GetMapping("/restaurantes/top2-por-nome")
	public List<Restaurante> restaurantesTop2PorNome(String nome) {
		return restaurantes.findTop2ByNomeContainingOrderByNomeAsc(nome);
	}
	
	
	@GetMapping("/restaurantes/count-por-cozinha")
	public int restaurantesCountPorCozinha(Long cozinhaId) {
		return restaurantes.countByCozinhaId(cozinhaId);
	}
	
	@GetMapping("/restaurantes/contar-native-query")
	public int contarComNativeQuery(Long cozinhaId) {
		return restaurantes.contarOcorrencias(cozinhaId);
	}
	
	@GetMapping("/restaurantes/filtrar-por-taxafrete-nativa")
	public List<Restaurante> filtrarRestaurantePorTaxafreteNativa(BigDecimal taxaInicial) {
		return restaurantes.filtrarPorTaxaFreteNativeQuery(taxaInicial);
	}
	
	@GetMapping("/restaurantes/filtrar-por-taxafrete-impl")
	public List<Restaurante> filtrarRestaurantePorTaxaFreteImpl(BigDecimal taxaInicial) {
		return restaurantes.listarRestaurantePorTaxaFreteProcedure(taxaInicial);
	}
	
	
	@GetMapping("/restaurantes/filtrar-por-taxafrete-named-query")
	public List<Restaurante> consultarPorNomeNamedQuery(@RequestParam("nome") String nome, 
			@Param("cozinhaID") Long cozinha) {
		return restaurantes.consultarPorNomeNamedQuery(nome, cozinha);
	}
	
	@GetMapping("/restaurantes/filtrar-por-taxafrete-jqpl")
	public PageResponse<RestauranteResumoModel> consultarPorTaxaFreteJQPL(@RequestParam(required = false) String nome, 
			@RequestParam(required = false) BigDecimal taxaFreteInicial, 
			@RequestParam(required = false) BigDecimal taxaFreteFinal, Pageable pageable) {
		
		var pageRestaurantes = restaurantes.find(nome, taxaFreteInicial, taxaFreteFinal, pageable);
		List<RestauranteResumoModel> restaurantes = restauranteAssembler
				.toCollectionModel(pageRestaurantes.getContent(), RestauranteResumoModel.class);
		
		return new PageResponse<>(
				new PageImpl<>(restaurantes, pageRestaurantes.getPageable(), pageRestaurantes.getTotalElements()
				));
	}
	
	@GetMapping("/restaurantes/por-nome-frete")
	public List<Restaurante> consultarPorNomeFrete(String nome, 
			BigDecimal taxaFreteInicial, BigDecimal taxaFreteFinal) {
		return restaurantes.findWithCriteria(nome, taxaFreteInicial, taxaFreteFinal);
	}
	
	@GetMapping("/restaurantes/com-frete-gratis")
	public List<Restaurante> consultarComFreteGratis(@RequestParam("nome") String nome) {
		var comNomeSemelhante = StringUtils.hasLength(nome) ? new RestauranteComNomeSemelhanteSpec(nome) : null ;
		return restaurantes.findAll(comNomeSemelhante);
	}
	
	@GetMapping("/restaurantes/consultar-spec-unica")
	public List<Restaurante> consultarComFreteGratis(String nome, BigDecimal taxaFreteInicial) {
		return restaurantes.findComFreteGratis(nome);
	}
	
	@GetMapping("/restaurantes/primeiro")
	public Restaurante restaurantePrimeiro() {
		Restaurante restaurante = restaurantes.buscarPrimeiro().orElse(null);
		return restaurante;
	}
	
	@PutMapping("/arquivo")
	public String enviarPlanilhar() {
		System.out.println(Runtime.getRuntime().availableProcessors());
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 10; i++) {
                	Cozinha cozinha = cozinhas.findById(1L).get();
                	cozinha.setNome("cozinha" + i);
                	cozinhas.save(cozinha);
                	try {
						TimeUnit.SECONDS.sleep(1);
						System.out.println(i);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
                }
            }
        }).start();
	    return "asdoas";
	}
	
	@PutMapping("/status")
	public String statusExecucao() {
		System.out.println(Thread.interrupted());
	    return "asdoas";
	}
	
	@PostMapping("/job")
	public void processarArquivo() {
//		String tmpdir = System.getProperty("java.io.tmpdir");
//		System.out.println(tmpdir);
		carregamentoArquivoService.importarDados("forma_pgto2.csv");
	}
}
