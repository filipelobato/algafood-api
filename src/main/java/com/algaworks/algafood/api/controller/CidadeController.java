package com.algaworks.algafood.api.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.algaworks.algafood.api.ResourceUriHelper;
import com.algaworks.algafood.api.assembler.CidadeModelAssembler;
import com.algaworks.algafood.api.model.CidadeModel;
import com.algaworks.algafood.api.model.input.CidadeInput;
import com.algaworks.algafood.api.openapi.controller.CidadeControllerOpenApi;
import com.algaworks.algafood.core.modelmapper.GenericInputDisassembler;
import com.algaworks.algafood.domain.exception.EstadoNaoEncontradoException;
import com.algaworks.algafood.domain.exception.NegocioException;
import com.algaworks.algafood.domain.model.Cidade;
import com.algaworks.algafood.domain.repository.CidadeRepository;
import com.algaworks.algafood.domain.service.CadastroCidadeService;

@RestController
@RequestMapping(path = "/cidades", produces = MediaType.APPLICATION_JSON_VALUE)
public class CidadeController implements CidadeControllerOpenApi {

	@Autowired
	private CidadeRepository cidadeRepository;

	@Autowired
	private CadastroCidadeService cadastroCidade;

	@Autowired
	private CidadeModelAssembler cidadeAssembler;

	@Autowired
	private GenericInputDisassembler<CidadeInput, Cidade> cidadeDisassembler;

	@GetMapping
	public CollectionModel<CidadeModel> listar() {
		List<Cidade> cidades = cidadeRepository.findAll();
		
		CollectionModel<CidadeModel> cidadesCollectionModel = cidadeAssembler.toCollectionModel(cidades);
		
		return cidadesCollectionModel;
	}

	@GetMapping("/{cidadeId}")
	public CidadeModel buscar(@PathVariable Long cidadeId) {
		Cidade cidade = cadastroCidade.buscarOuFalhar(cidadeId);

		CidadeModel cidadeModel = cidadeAssembler.toModel(cidade);
		
		
//		Link link = linkTo(methodOn(CidadeController.class)
//				.buscar(cidadeModel.getId()))
//				.withSelfRel();
		
//		cidadeModel.add(linkTo(methodOn(CidadeController.class)
//				.buscar(cidadeModel.getId())).withSelfRel());
//		
//		cidadeModel.add(linkTo(methodOn(CidadeController.class)
//				.listar()).withRel("cidades"));
//		
//		cidadeModel.getEstado().add(linkTo(methodOn(EstadoController.class)
//				.buscar(cidadeModel.getEstado().getId())).withSelfRel());
		
		// abandonando o uso de Slash (barra) para usar o builder methodOn

//		cidadeModel.add(linkTo(CidadeController.class).slash(cidadeModel.getId()).withSelfRel());

//		cidadeModel.add(linkTo(CidadeController.class).withRel("cidades"));

//		cidadeModel.getEstado().add(
//				linkTo(EstadoController.class).slash(cidadeModel.getEstado().getId()).withSelfRel());
		
//		Foi removido para que se colocasse os links dinamicamente.
//		cidadeModel.add(Link.of("http://api.algafood.local:8080/cidades/1"));
//		cidadeModel.add(Link.of("http://api.algafood.local:8080/cidades/1", IanaLinkRelations.SELF));

//		cidadeModel.add(Link.of("http://api.algafood.local:8080/cidades" , "cidades"));
//		cidadeModel.getEstado().add(Link.of("http://api.algafood.local:8080/estados/1"));

//		cidadeModel.add(Link.of("cidades", "http://api.algafood.local:8080/estados/1"));
//		cidadeModel.add(Link.of("http://api.algafood.local:8080/cidades", IanaLinkRelations.COLLECTION));

		return cidadeModel;
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public CidadeModel adicionar(@RequestBody @Valid CidadeInput cidadeInput) {

		try {
			Cidade cidade = cidadeDisassembler.toDomain(cidadeInput, Cidade.class);

			cidade = cadastroCidade.salvar(cidade);

			CidadeModel cidadeModel = cidadeAssembler.toModel(cidade);

			ResourceUriHelper.addUriInResponseHeader(cidadeModel.getId());

			return cidadeModel;
		} catch (EstadoNaoEncontradoException e) {
			throw new NegocioException(e.getMessage(), e);
		}
	}

	@PutMapping("/{cidadeId}")
	public CidadeModel atualizar(@RequestBody @Valid CidadeInput cidadeInput, @PathVariable Long cidadeId) {

		Cidade cidadeAtual = cadastroCidade.buscarOuFalhar(cidadeId);

		try {
			cidadeDisassembler.copyToDomainObject(cidadeInput, cidadeAtual);

			return cidadeAssembler.toModel(cadastroCidade.salvar(cidadeAtual));
		} catch (EstadoNaoEncontradoException e) {
			throw new NegocioException(e.getMessage(), e);
		}
	}

	@DeleteMapping("/{cidadeId}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remover(@PathVariable Long cidadeId) {
		cadastroCidade.excluir(cidadeId);
	}

}
