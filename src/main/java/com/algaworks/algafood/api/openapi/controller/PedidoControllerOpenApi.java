package com.algaworks.algafood.api.openapi.controller;

import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.PagedModel;

import com.algaworks.algafood.api.exceptionhandler.Problem;
import com.algaworks.algafood.api.model.PedidoModel;
import com.algaworks.algafood.api.model.PedidoResumoModel;
import com.algaworks.algafood.api.model.input.PedidoInput;
import com.algaworks.algafood.domain.filter.PedidoFilter;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = "Pedidos")
public interface PedidoControllerOpenApi {

	@ApiImplicitParams({
		@ApiImplicitParam(value = "Nomes das propriedades para filtrar na resposta, separados por vírgula",
				name= "campos", paramType = "query", type = "string")
	})
	@ApiOperation("Pesquisa os pedidos")
	PagedModel<PedidoResumoModel> pesquisar(PedidoFilter filtro, Pageable pageable);
	
	@ApiImplicitParams({
		@ApiImplicitParam(value = "Nomes das propriedades para filtrar na resposta, separados por vírgula",
				name= "campos", paramType = "query", type = "string")
	})
	@ApiOperation("Busca um pedido por código do pedido")
	@ApiResponses({
		@ApiResponse(code = 404, message = "Pedido não encontrado", response = Problem.class)
	})
	PedidoModel buscar(
			@ApiParam(value = "Código do pedido", example = "24c6ec01-afb7-11ed-b94e-0242ac110002", required = true) 
			String codigoPedido);

	@ApiImplicitParams({
		@ApiImplicitParam(value = "Nomes das propriedades para filtrar na resposta, separados por vírgula",
				name= "campos", paramType = "query", type = "string")
	})
	@ApiOperation("Registra um novo pedido")
	@ApiResponses({
		@ApiResponse(code = 201, message = "Pedido registrado")
	})
	PedidoModel adicionar(
			@ApiParam(name = "corpo", value = "Representação de uma novo pedido")
			 PedidoInput pedidoInput);
}
