package com.algaworks.algafood.api.openapi.controller;

import java.util.List;

import com.algaworks.algafood.api.model.GrupoModel;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(tags = "Usuários")
public interface UsuarioGrupoControllerOpenApi {
	
	@ApiOperation("Lista todos os grupos por ID do usuário")
	public List<GrupoModel> listarGrupos(
			@ApiParam(value = "Id do usuário", example = "1", required = true) 
			Long usuarioId);
	
	@ApiOperation("Associa usuário ao grupo por ID do usuário e ID do grupo")
	public void associar(
			@ApiParam(value = "Id do usuário", example = "1", required = true) 
			Long usuarioId, 
			@ApiParam(value = "Id do grupo", example = "1", required = true)
			Long grupoId);
	
	@ApiOperation("Desassocia usuário do grupo por ID do usuário e ID do grupo")	
	public void desassociar(
			@ApiParam(value = "Id do usuário", example = "1", required = true) 
			Long usuarioId, 
			@ApiParam(value = "Id do grupo", example = "1", required = true)
			Long grupoId);
}
