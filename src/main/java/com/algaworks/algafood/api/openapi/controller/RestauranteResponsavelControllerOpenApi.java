package com.algaworks.algafood.api.openapi.controller;

import org.springframework.hateoas.CollectionModel;

import com.algaworks.algafood.api.exceptionhandler.Problem;
import com.algaworks.algafood.api.model.UsuarioModel;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = "Restaurantes")
public interface RestauranteResponsavelControllerOpenApi {

	@ApiOperation("Lista os responsáveis do restaurante por ID do restaurante")
	CollectionModel<UsuarioModel> listar(
			@ApiParam(value = "ID do restaurante", example = "1", required = true)
			Long restauranteId);
	
	@ApiOperation("Associa restaurante com um usuário responsável")
	@ApiResponses({
		@ApiResponse(code = 204, message = "Usuário associado com sucesso"),
        @ApiResponse(code = 404, message = "Restaurante ou usuário não encontrado", 
        response = Problem.class)
		
	})
	void associar(
			@ApiParam(value = "ID do restaurante", example = "1", required = true)
			Long restauranteId, 
			@ApiParam(value = "ID do responsável", example = "1", required = true)
			Long responsavelId);

	@ApiOperation("Desassocia restaurante com um usuário responsável")
	@ApiResponses({
		@ApiResponse(code = 204, message = "Usuário associado com sucesso"),
        @ApiResponse(code = 404, message = "Restaurante ou usuário não encontrado", 
        response = Problem.class)
		
	})
	void desassociar(
			@ApiParam(value = "ID do restaurante", example = "1", required = true)
			Long restauranteId, 
			@ApiParam(value = "ID do responsável", example = "1", required = true)
			Long responsavelId);
	
}
