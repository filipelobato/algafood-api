package com.algaworks.algafood.api.openapi.controller;

import org.springframework.http.ResponseEntity;

import com.algaworks.algafood.api.exceptionhandler.Problem;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = "Pedidos")
public interface FluxoPedidoControllerOpenApi {

	@ApiOperation("Aceita/confirma o pedido registrado")
	@ApiResponses({
		@ApiResponse(code = 204, message = "Pedido confirmado com sucesso"),
		@ApiResponse(code = 404, message = "Pedido não encontrado", response = Problem.class)
	})
	ResponseEntity<Void> confirmar(
			@ApiParam(value = "Código do pedido", example = "24c6ec01-afb7-11ed-b94e-0242ac110002", required = true) 
			String codigoPedido);
	
	@ApiOperation("Cancela o pedido registrado")
	@ApiResponses({
		@ApiResponse(code = 204, message = "Pedido cancelado com sucesso"),
		@ApiResponse(code = 404, message = "Pedido não encontrado", response = Problem.class)
	})
	ResponseEntity<Void> cancelar(
			@ApiParam(value = "Código do pedido", example = "24ca7a8c-afb7-11ed-b94e-0242ac110002", required = true)
			String codigoPedido);
	
	@ApiOperation("Registra a entrega pedido")
	@ApiResponses({
		@ApiResponse(code = 204, message = "Entrega de pedido registrada com sucesso"),
		@ApiResponse(code = 404, message = "Pedido não encontrado", response = Problem.class)
	})
	ResponseEntity<Void> entregar(
			@ApiParam(value = "Código do pedido", example = "24c6ec01-afb7-11ed-b94e-0242ac110002", required = true)
			String codigoPedido);
}
