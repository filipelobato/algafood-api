package com.algaworks.algafood.api.openapi.controller;

import java.util.List;

import org.springframework.hateoas.CollectionModel;

import com.algaworks.algafood.api.exceptionhandler.Problem;
import com.algaworks.algafood.api.model.UsuarioModel;
import com.algaworks.algafood.api.model.input.SenhaInput;
import com.algaworks.algafood.api.model.input.UsuarioComSenhaInput;
import com.algaworks.algafood.api.model.input.UsuarioInput;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = "Usuários")
public interface UsuarioControllerOpenApi {

	@ApiOperation("Lista todo os usuários")
	public CollectionModel<UsuarioModel> listar();
	
	@ApiOperation("Cadastra usuário")
	@ApiResponse(code = 201, message = "Usuário cadastrado")
	public UsuarioModel adicionar(
			@ApiParam(name = "corpo", value = "Representação de um novo usuário") 
			UsuarioComSenhaInput usuarioInput);
	
	@ApiOperation("Busca um usuário por ID")
	@ApiResponses({
		@ApiResponse(code = 404, message = "Usuário não encontrado", response = Problem.class),
		@ApiResponse(code = 400, message = "ID do usuário inválido", response = Problem.class),
	})
	public UsuarioModel buscar(
			@ApiParam(value= "ID do usuário", example = "1", required = true)
			Long usuarioId);
	
	@ApiOperation("Atualiza um usuário por ID	")
	@ApiResponses({
		@ApiResponse(code = 200, message = "Usuário atualizado"),
		@ApiResponse(code = 404, message = "Usuário não encontrado", response = Problem.class),
	})
	public UsuarioModel atualizar(
			@ApiParam(value = "ID do usuário", example = "1", required = true)
			Long usuarioId,
			@ApiParam(name = "corpo", value = "Representação de um usuário com os novos dados") 
			UsuarioInput usuarioInput);
	
	@ApiOperation("Exclui usuário")
	@ApiResponses({
		@ApiResponse(code = 204, message = "Usuário excluído"),
		@ApiResponse(code = 404, message = "Usuário não encontrado", response = Problem.class),
		@ApiResponse(code = 400, message = "ID do usuário inválido", response = Problem.class)
	})
	public void excluir(
			@ApiParam(value = "ID do usuário", example = "4", required = true) 
			Long usuarioId);

	@ApiOperation("Altera senha do usuário")
	@ApiResponses({
		@ApiResponse(code = 200, message = "Senha do usuário atualizada"),
		@ApiResponse(code = 404, message = "Usuário não encontrado", response = Problem.class),
	})
	public void alterarSenha(
			@ApiParam(name = "corpo", value = "Representação com senha atual e nova senha do usuário")  
			SenhaInput senha,
			@ApiParam(value = "ID do usuário", example = "1", required = true)
			Long usuarioId);
	
}