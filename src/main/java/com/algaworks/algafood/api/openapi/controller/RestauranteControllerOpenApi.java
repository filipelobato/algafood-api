package com.algaworks.algafood.api.openapi.controller;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.hateoas.CollectionModel;
import org.springframework.http.ResponseEntity;

import com.algaworks.algafood.api.exceptionhandler.Problem;
import com.algaworks.algafood.api.model.RestauranteApenasNomeModel;
import com.algaworks.algafood.api.model.RestauranteBasicoModel;
import com.algaworks.algafood.api.model.RestauranteModel;
import com.algaworks.algafood.api.model.input.RestauranteInput;
import com.algaworks.algafood.api.openapi.model.RestauranteBasicoModelOpenApi;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = "Restaurantes")
public interface RestauranteControllerOpenApi {
	
	@ApiOperation(value = "Lista restaurantes", response = RestauranteBasicoModelOpenApi.class)
	@ApiImplicitParams({
		@ApiImplicitParam(value = "Nome da projeção de pedidos", allowableValues = "apenas-nome",
				name = "projecao", paramType = "query", type = "string")
	})
    CollectionModel<RestauranteBasicoModel> listar();
	
	@ApiOperation(value = "Lista restaurantes", hidden = true)
    CollectionModel<RestauranteApenasNomeModel> listarApenasNomes();
    
	@ApiOperation(value = "Busca o restaurante por ID")
	@ApiResponses({
		@ApiResponse(code = 400, message = "ID do restaurante inválido", response = Problem.class),
		@ApiResponse(code = 404, message = "Restaurante não encontrado", response = Problem.class)
	})
	RestauranteModel buscar(
			@ApiParam(value = "ID do restaurante", example = "1", required = true) 
			Long restauranteId);
    
	@ApiOperation(value = "Cadastra um novo restaurante")
	@ApiResponses({
		@ApiResponse(code = 201, message = "Restaurante registrado")
	})
    RestauranteModel adicionar(
    		@ApiParam(name = "corpo", value = "Representação de um novo restaurante")
    		RestauranteInput restauranteInput
    		);
    
	@ApiOperation("Atualiza um restaurante por ID")
	@ApiResponses({
		@ApiResponse(code = 200, message = "Restaurante atualizado"),
		@ApiResponse(code = 404, message = "Restaurante não encontrado", response = Problem.class)
	})
    RestauranteModel atualizar(
    		@ApiParam(value = "ID do restaurante", example = "1", required = true) 
    		Long restauranteId,
			@ApiParam(name = "corpo", value = "Representação de um restaurante com os novos dados")
			RestauranteInput restauranteInput);
    
	@ApiOperation("Atualiza um restaurante por ID somente nas propriedades especificadas no JSON")
	RestauranteModel atualizarParcial(
			@ApiParam(value = "ID do restaurante", example = "1", required = true) 
			Long restauranteId, 
			@ApiParam(name = "corpo", value = "Conjunto somente das chave e valor da representação "
					+ "de restaurante a ser atualizado. Exemplo: \n<code>{\n"
					+ "  \"nome\": \"novoNome\",\n"
					+ "  \"taxaFrete\": 12.00\n"
					+ "}</code>", type = "object" )
			HashMap<String, Object> campos, HttpServletRequest request);
	
	@ApiOperation("Ativa um restaurante por ID")
	@ApiResponse(code = 404, message = "Restaurante não encontrado", response = Problem.class)
	ResponseEntity<Void> ativar(
			@ApiParam(value = "ID do restaurante", example = "1", required = true) 
			Long restauranteId);
	
	@ApiOperation("Inativa uma cozinha por ID")
	@ApiResponses({
		@ApiResponse(code = 400, message = "ID do restaurante inválido", response = Problem.class),
		@ApiResponse(code = 404, message = "Restaurante não encontrado", response = Problem.class)
	})
	ResponseEntity<Void> inativar(
			@ApiParam(value = "ID do restaurante", example = "1", required = true) 
			Long restauranteId);
	
	@ApiOperation("Ativa diversas cozinhas por lista de IDs")
	@ApiResponses({
		@ApiResponse(code = 400, message = "ID(s) do restaurante(s) inválido", response = Problem.class),
		@ApiResponse(code = 404, message = "Restaurante(s) não encontrado(s)", response = Problem.class)
	})
	void ativarMultiplos(
			@ApiParam(value = "IDs dos restaurantes", example = "[1, 2, 9]", required = true) 
			List<Long> restauranteIds);
	
	@ApiOperation("Inativa diversas cozinhas por lista de IDs")
	@ApiResponses({
		@ApiResponse(code = 400, message = "ID(s) do restaurante(s) inválido", response = Problem.class),
		@ApiResponse(code = 404, message = "Restaurante(s) não encontrado(s)", response = Problem.class)
	})
	void inativarMultiplos(
			@ApiParam(value = "IDs dos restaurantes", example = "[1, 2, 9]", required = true)
			List<Long> restauranteIds);

    @ApiOperation("Abre um restaurante por ID")
    @ApiResponses({
        @ApiResponse(code = 204, message = "Restaurante aberto com sucesso"),
        @ApiResponse(code = 404, message = "Restaurante não encontrado", response = Problem.class)
    })
    ResponseEntity<Void> abrir(
			@ApiParam(value = "ID do restaurante", example = "1", required = true) 
			Long restauranteId);
    
    @ApiOperation("Fecha um restaurante por ID")
    @ApiResponses({
        @ApiResponse(code = 204, message = "Restaurante fechado com sucesso"),
        @ApiResponse(code = 404, message = "Restaurante não encontrado", response = Problem.class)
    })
    ResponseEntity<Void> fechar(
			@ApiParam(value = "ID do restaurante", example = "1", required = true) 
			Long restauranteId);
}
