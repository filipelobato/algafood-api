package com.algaworks.algafood.api.openapi.controller;

import org.springframework.hateoas.CollectionModel;

import com.algaworks.algafood.api.exceptionhandler.Problem;
import com.algaworks.algafood.api.model.EstadoModel;
import com.algaworks.algafood.api.model.input.EstadoInput;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = "Estados")
public interface EstadoControllerOpenApi {
	
	@ApiOperation("Lista todos os estados")
	CollectionModel<EstadoModel> listar();
	
	@ApiOperation("Busca o Estado por ID")
	@ApiResponses({
		@ApiResponse(code = 400, message =  "ID do estado inválido", response = Problem.class),
		@ApiResponse(code = 404, message =  "Estado não encontrado", response = Problem.class),
		
	})
	EstadoModel buscar(
			@ApiParam(value = "ID do estado", example = "1", required = true)
			Long estadoId);
	
	@ApiResponse(code = 201, message =  "Estado cadastrado")
	@ApiOperation("Cadastra um novo estado")
	EstadoModel adicionar(
			@ApiParam(name = "corpo", value = "Representação de um novo estado")
			EstadoInput estadoInput);
	
	@ApiOperation("Atualiza um estado por ID")
	@ApiResponses({
		@ApiResponse(code = 200, message =  "Estado atualizado"),
		@ApiResponse(code = 404, message =  "Estado não encontrado", response = Problem.class),
		
	})
	EstadoModel atualizar(
			@ApiParam(value = "ID do estado", example = "1", required = true) 
			Long estadoId,
			@ApiParam(name = "corpo", value = "Representação de um novo estado")
			EstadoInput estadoInput);
	
	@ApiOperation("Remove um estado por ID")
	@ApiResponses({
		@ApiResponse(code = 204, message =  "Estado excluído"),
		@ApiResponse(code = 404, message =  "Estado não encontrado", response = Problem.class),
		
	})
	void remover(
			@ApiParam(value = "ID do estado", example = "1", required = true) 
			Long estadoId);
}
