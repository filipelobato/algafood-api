package com.algaworks.algafood.api.assembler;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.stereotype.Component;

import com.algaworks.algafood.api.AlgaLinks;
import com.algaworks.algafood.api.controller.CidadeController;
import com.algaworks.algafood.api.model.CidadeModel;
import com.algaworks.algafood.core.modelmapper.GenericRepresentationModelAssembler;
import com.algaworks.algafood.core.modelmapper.Mapper;
import com.algaworks.algafood.domain.model.Cidade;

@Component
public class CidadeModelAssembler extends GenericRepresentationModelAssembler<CidadeModel, Cidade, CidadeController> {

	@Autowired
	private AlgaLinks algaLinks;

	public CidadeModelAssembler(Mapper mapper) {
		super(mapper, CidadeController.class, CidadeModel.class);
	}

	@Override
	public List<Link> constructLinks(Cidade entityObject) {
		return Arrays.asList(algaLinks.linkToCidade(entityObject.getId()),
							 algaLinks.linkToCidade("cidades")
				);
	}

	@Override
	public Link constructCollectionLink() {
		return algaLinks.linkToCidade();
	} 
	
	@Override
	public CidadeModel toModel(Cidade entityObject) {
		var cidadeModel = super.toModel(entityObject);
		
		if (cidadeModel.getEstado() != null) {
            cidadeModel.getEstado().add(algaLinks.linkToEstado(cidadeModel.getEstado().getId()));
        }
		
		if (cidadeModel.getEstado() != null) {
            cidadeModel.getEstado().add(algaLinks.linkToEstados("estados"));
        }
		
		return cidadeModel;
	}
	
}