package com.algaworks.algafood.api.assembler;

import java.util.List;

public interface Converter<T, S, U> {
    
	public T toDomain(U inputDto);

    public S toDto(T domain);

    public List<S> toCollectionDTO(List<T> list);
}
