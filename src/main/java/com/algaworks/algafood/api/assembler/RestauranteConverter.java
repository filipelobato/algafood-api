package com.algaworks.algafood.api.assembler;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.algaworks.algafood.api.model.CozinhaModel;
import com.algaworks.algafood.api.model.RestauranteModel;
import com.algaworks.algafood.api.model.input.RestauranteInput;
import com.algaworks.algafood.domain.model.Cozinha;
import com.algaworks.algafood.domain.model.Restaurante;

@Component
public class RestauranteConverter implements Converter<Restaurante, RestauranteModel, RestauranteInput> {

    @Override
    public Restaurante toDomain(RestauranteInput dto) {
        Restaurante restaurante = new Restaurante();
        restaurante.setNome(dto.getNome());
        restaurante.setTaxaFrete(dto.getTaxaFrete());

        Cozinha cozinha = new Cozinha();
        cozinha.setId(dto.getCozinha().getId());

        restaurante.setCozinha(cozinha);

        return restaurante;
    }

    @Override
    public RestauranteModel toDto(Restaurante domain) {
    	RestauranteModel restauranteModel = new RestauranteModel();
        restauranteModel.setId(domain.getId());
        restauranteModel.setNome(domain.getNome());
        restauranteModel.setTaxaFrete(domain.getTaxaFrete());

        CozinhaModel cozinhaModel = new CozinhaModel();
        cozinhaModel.setId(domain.getCozinha().getId());
        cozinhaModel.setNome(domain.getCozinha().getNome());

        restauranteModel.setCozinha(cozinhaModel);

        return restauranteModel;
    }

    @Override
    public List<RestauranteModel> toCollectionDTO(List<Restaurante> list) {
        return list.stream().map(restaurante -> toDto(restaurante)).collect(Collectors.toList());
    }
}