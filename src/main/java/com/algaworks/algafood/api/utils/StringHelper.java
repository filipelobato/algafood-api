package com.algaworks.algafood.api.utils;

import java.text.Normalizer;

import org.springframework.util.StringUtils;

public class StringHelper {
	
	/**
	 * Substitui os caracteres com acentuações em caracteres sem acentuações. 
	 *  <pre><code>Ex: StringHelper.removerAcentuacoes(string)</code></pre>
	 * @param 
	 * @return String 
	 */
	public static String removerAcentuacoes(String value) {
		return !StringUtils.hasLength(value) ? "" :
				Normalizer.normalize(value, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
	}
}
