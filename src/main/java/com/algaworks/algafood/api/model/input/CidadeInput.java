package com.algaworks.algafood.api.model.input;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class CidadeInput {
	// OBS: o ApiModelProperty default substitui o required do SpringFox-Bean-Validators
//	@ApiModelProperty(example = "São Luís", required = true)
//	@NotBlank
	private String nome;
	
	@Valid
	@NotNull
	private EstadoIdInput estado;
}
