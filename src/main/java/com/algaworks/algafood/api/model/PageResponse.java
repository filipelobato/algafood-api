package com.algaworks.algafood.api.model;

import java.util.List;

import org.springframework.data.domain.Page;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class PageResponse<T> {
	private List<T> content;
	private int totalElements;
	private int totalPages;
	private int size;
	
	public PageResponse(Page<T> page) {
		this.content = page.getContent();
		this.totalElements = (int) page.getTotalElements();
		this.totalPages = page.getTotalPages();
		this.size = page.getSize();
	}
}
