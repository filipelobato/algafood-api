package com.algaworks.algafood.api.model;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Relation(collectionRelation = "cidades")
public class CidadeResumoModel extends RepresentationModel<CidadeResumoModel> {
	@ApiModelProperty(example = "1")
	private Long id;

	@ApiModelProperty(example = "Niterói")
	private String nome;
	
	@ApiModelProperty(example = "Rio de Janeiro")
	private String estado;
}
