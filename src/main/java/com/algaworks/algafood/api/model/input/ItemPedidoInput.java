package com.algaworks.algafood.api.model.input;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ItemPedidoInput {
	
	@NotNull
	@ApiModelProperty(example = "1", required = true)
	private Long produtoId;
	
	@NotNull
	@Positive
	@ApiModelProperty(value = "Quantidade de itens em número inteiro maior que 0", example = "2", required = true)
	private Integer quantidade;
	
	@ApiModelProperty(example = "Ao ponto", required = true)
	private String observacao;
}
