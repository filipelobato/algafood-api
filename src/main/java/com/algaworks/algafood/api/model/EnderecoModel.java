package com.algaworks.algafood.api.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class EnderecoModel {
	
	@ApiModelProperty(example = "65010-620")
	private String cep;
	
	@ApiModelProperty(example = "Rua da Giz")
	private String logradouro;
	
	@ApiModelProperty(example = "784")
	private String numero;

	@ApiModelProperty(example = "Em frente a escola X")
	private String complemento;
	
	@ApiModelProperty(example = "Centro")
	private String bairro;
	
	private CidadeResumoModel cidade;
//	private String nomeCidade;
//	@JsonProperty("nomeEstado")
//	private String cidadeNomeEstado;
}
