package com.algaworks.algafood.api.model.input;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class EnderecoInput {
	
	@NotBlank
	@ApiModelProperty(example = "65010-620", required = true)
	private String cep;
	
	@NotBlank
	@ApiModelProperty(example = "Rua da Giz", required = true)
	private String logradouro;
	
	@NotBlank
	@ApiModelProperty(example = "784", required = true)
	private String numero;
	
	@ApiModelProperty(example = "Em frente a escola X", required = true)
	private String complemento;
	
	@NotBlank
	@ApiModelProperty(example = "Centro", required = true)
	private String bairro;
	
	@Valid
	@NotNull
	private CidadeIdInput cidade;

}
