package com.algaworks.algafood.api.model.mixin;

import java.util.ArrayList;
import java.util.List;

import com.algaworks.algafood.domain.model.Restaurante;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

public abstract class CozinhaMixin {
	
	@JsonIgnore
	@JsonIgnoreProperties(value = "cozinha")
	private List<Restaurante> restaurantes = new ArrayList<>();
}
