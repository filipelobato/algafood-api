package com.algaworks.algafood.api.model.input;

import com.sun.istack.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@ApiModel("CozinhaId")
@Getter @Setter
public class CozinhaIdInput {
	
	@NotNull
	@ApiModelProperty(example = "1", required = true)
	private Long id;
}
