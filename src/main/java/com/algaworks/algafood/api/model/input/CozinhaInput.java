package com.algaworks.algafood.api.model.input;

import javax.validation.constraints.NotBlank;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@ApiModel("Representação de uma cozinha")
@Getter @Setter
public class CozinhaInput {

	@ApiModelProperty(example = "Tailandesa", required = true)
	@NotBlank
	private String nome;
}
