package com.algaworks.algafood.api.model.input;

import javax.validation.constraints.NotBlank;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@ApiModel("Representação de uma forma de pagamento")
@Getter @Setter
public class FormaPagamentoInput {

	@ApiModelProperty(example = "Crédito", required = true)
	@NotBlank
	private String descricao;
}
