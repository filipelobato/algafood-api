package com.algaworks.algafood.api.model;

import java.math.BigDecimal;
import java.time.OffsetDateTime;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

//@JsonFilter("pedidoFilter")
@Getter @Setter
@Relation(collectionRelation = "pedidos")
public class PedidoResumoModel extends RepresentationModel<PedidoResumoModel> {
	
	@ApiModelProperty(example = "24c6ec01-afb7-11ed-b94e-0242ac110002")
	private String codigo;
	
	@ApiModelProperty(example = "298.90")
	private BigDecimal subtotal;
	
	@ApiModelProperty(example = "10")
	private BigDecimal taxaFrete;
	
	@ApiModelProperty(example = "308.90")
	private BigDecimal valorTotal;
	
	@ApiModelProperty(example = "CRIADO") 
	private String status;
	
	@ApiModelProperty(example = "2019-12-01T20:34:04Z")
	private OffsetDateTime dataCriacao;
	
	@ApiModelProperty(example = "2019-12-01T20:35:10Z")
	private OffsetDateTime dataConfirmacao;

	@ApiModelProperty(example = "2019-12-01T20:55:30Z")
	private OffsetDateTime dataEntrega;

	@ApiModelProperty(example = "2019-12-01T20:35:00Z")
	private OffsetDateTime dataCancelamento;
	
	private RestauranteApenasNomeModel restaurante;
	
	@ApiModelProperty(example = "Maria Joaquina")
	private String nomeCliente;
}
