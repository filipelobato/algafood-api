package com.algaworks.algafood.api.example;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:test/configuration-file.properties")
@ConfigurationProperties(prefix = "configuration-file")
public class ConfigurationFile {

	@Value("${configuration-file.testValue:'default value to @Value annotation'")
	private String testValue;
	
	private String testConfiguration;

	// Getters and Setters are mandatory
	public String getTestValue() {
		return testValue;
	}

	public void setTestValue(String testValue) {
		this.testValue = testValue;
	}

	public String getTestConfiguration() {
		return testConfiguration;
	}

	public void setTestConfiguration(String testConfiguration) {
		this.testConfiguration = testConfiguration;
	}
	
}
